﻿using Grpc.Core;

namespace SiLA2.Communication.Protobuf
{
    public class ProtobufMarshaller<T>
    {
        public static readonly Marshaller<T> Default = new Marshaller<T>(ToByteArray, FromByteArray);

        public static T FromByteArray(byte[] data)
        {
            return ByteSerializer.FromByteArray<T>(data);
        }

        public static byte[] ToByteArray(T instance)
        {
            return ByteSerializer.ToByteArray(instance);
        }

        public static Marshaller<T> FromByteSerializer(ByteSerializer<T> serializer)
        {
            return new Marshaller<T>(serializer.Serializer, serializer.Deserializer);
        }
    }
}