﻿using SiLA2.Communication.Protobuf;

namespace SiLA2.Communication.Services
{
    public class PayloadFactory : IPayloadFactory
    {
        public Tuple<Type, Type> GetCommandPayloadTypes(Feature feature, string operation)
        {
            var propertyMapRequest = new Dictionary<string, Type>();
            var propertyMapResponse = new Dictionary<string, Type>();

            var command = feature.GetDefinedCommands().SingleOrDefault(c => c.Identifier == operation);

            if(command != null && command.Parameter != null)
            {
                foreach (var parameter in command.Parameter)
                {
                    Type parameterType = GetProtobufType(parameter.DataType, feature, ref operation);
                    propertyMapRequest.Add(parameter.Identifier, parameterType);
                }
            }
            
            if(command != null && command.Response != null)
            {
                foreach (var responseItem in command.Response)
                {
                    Type responseType = GetProtobufType(responseItem.DataType, feature, ref operation);
                    propertyMapResponse.Add(responseItem.Identifier, responseType);
                }
            }
            
            var dynProtoTypeRequest = "DynamicProtobufRequest".Build(propertyMapRequest);
            var dynProtoTypeResponse = "DynamicProtobufResponse".Build(propertyMapResponse);

            return new Tuple<Type, Type>(dynProtoTypeRequest, dynProtoTypeResponse);
        }

        public Tuple<Type, Type> GetPropertyPayloadTypes(Feature feature, string property)
        {
            var propertyMapRequest = new Dictionary<string, Type>();
            var propertyMapResponse = new Dictionary<string, Type>();

            var definedProperty = feature.GetDefinedProperties().SingleOrDefault(c => c.Identifier == property);

            if (definedProperty != null)
            {
                Type responseType = GetProtobufType(definedProperty.DataType, feature, ref property);
                propertyMapResponse.Add(definedProperty.Identifier, responseType);
            }

            var dynProtoTypeRequest = "DynamicProtobufRequest".Build(propertyMapRequest);
            var dynProtoTypeResponse = "DynamicProtobufResponse".Build(propertyMapResponse);

            return new Tuple<Type, Type>(dynProtoTypeRequest, dynProtoTypeResponse);
        }

        private Type GetProtobufType(DataTypeType dataType, Feature feature, ref string property, string structureIdentifier = null)
        {
            if(dataType.Item.GetType() == typeof(BasicType))
            {
                var basicType = Enum.Parse(typeof(BasicType), dataType.Item.ToString());
                switch (basicType)
                {
                    case BasicType.String:
                        return typeof(Sila2.Org.Silastandard.Protobuf.String);
                    case BasicType.Binary:
                        return typeof(Sila2.Org.Silastandard.Protobuf.Binary);
                    case BasicType.Boolean:
                        return typeof(Sila2.Org.Silastandard.Protobuf.Boolean);
                    case BasicType.Date:
                        return typeof(Sila2.Org.Silastandard.Protobuf.Date);
                    case BasicType.Real:
                        return typeof(Sila2.Org.Silastandard.Protobuf.Real);
                    case BasicType.Integer:
                        return typeof(Sila2.Org.Silastandard.Protobuf.Integer);
                    case BasicType.Time:
                        return typeof(Sila2.Org.Silastandard.Protobuf.Time);
                    case BasicType.Timestamp:
                        return typeof(Sila2.Org.Silastandard.Protobuf.Timestamp);
                    case BasicType.Any:
                        return typeof(Sila2.Org.Silastandard.Protobuf.Any);
                    default:
                        throw new ArgumentException($"Unknown BasicType '{basicType}'");
                }
                
            }
            else if(dataType.Item.GetType() == typeof(ConstrainedType))
            {
                return GetProtobufType(((ConstrainedType)dataType.Item).DataType, feature, ref property);
            }
            else if (dataType.Item.GetType() == typeof(ListType))
            {
                var listItemType = GetProtobufType(((ListType)dataType.Item).DataType, feature, ref property);
                return listItemType.CreateRepeatedFieldType();
            }
            else if (dataType.Item.GetType() == typeof(StructureType))
            {
                var elements = ((StructureType)dataType.Item).Element;

                var propertyMap = new Dictionary<string, Type>();
                foreach(var element in elements) 
                {
                    propertyMap.Add(element.Identifier, GetProtobufType(element.DataType, feature, ref property));
                }
                
                var structureType = structureIdentifier.Build(propertyMap);
                var structureValueType = property.Build(new Dictionary<string, Type> { { structureIdentifier, structureType} });
                
                return structureValueType;
            }
            else if (dataType.Item is string)
            {
                var definedDataTypes = feature.GetDefinedDataTypes();
                var definedType = definedDataTypes.SingleOrDefault(t => t.Identifier.Equals(dataType.Item.ToString()));

                if (definedType != null)
                {
                    return GetProtobufType(definedType.DataType, feature, ref property, definedType.Identifier);
                }
                throw new ArgumentException($"Unknown DefinedDataType '{dataType.Item}'");
            }

            throw new ArgumentException($"Unknown DataType '{dataType.Item.GetType()}'");
        }
    }
}
