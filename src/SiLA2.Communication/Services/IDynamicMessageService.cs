﻿using Grpc.Net.Client;
using Sila2.Org.Silastandard;

namespace SiLA2.Communication.Services
{
    public interface IDynamicMessageService
    {
        object GetUnobservableProperty(string propertyName, GrpcChannel channel, Feature feature);
        IAsyncEnumerable<object> SubcribeObservableProperty(string propertyName, GrpcChannel channel, Feature feature);
        object ExecuteUnobservableCommand(string operationName, GrpcChannel channel, Feature feature, IDictionary<string, object> payloadMap = null);
        Tuple<CommandConfirmation, IAsyncEnumerable<ExecutionInfo>, Type> ExecuteObservableCommand(string operationName, GrpcChannel channel, Feature feature, IDictionary<string, object> payloadMap = null);
        object GetObservableCommandResult(CommandExecutionUUID cmdId, string operationName, GrpcChannel channel, Feature feature, Type responseType);
    }
}