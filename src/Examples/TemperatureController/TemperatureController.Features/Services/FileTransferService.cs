﻿using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.De.Chamundi.Util.Filetransfer.V1;
using SiLA2;
using SiLA2.Server;
using SiLA2.Server.Utils;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using static Sila2.De.Chamundi.Util.Filetransfer.V1.FileTransfer;

namespace TemperatureController.Features.Services
{
    public class FileTransferService : FileTransferBase
    {
        private Feature _silaFeature;
        private readonly IBinaryUploadRepository _binaryUploadRepository;
        private readonly IBinaryDownloadRepository _binaryDownloadRepository;
        private readonly ILogger<FileTransferService> _logger;

        public FileTransferService(ISiLA2Server silaServer, IBinaryUploadRepository binaryUploadRepository, IBinaryDownloadRepository binaryDownloadRepository, ILogger<FileTransferService> logger)
        {
            _silaFeature = silaServer.ReadFeature(Path.Combine("Features", "FileTransfer-v1_0.sila.xml"));
            _binaryUploadRepository = binaryUploadRepository;
            _binaryDownloadRepository = binaryDownloadRepository;
            _logger = logger;
        }

        public override async Task<SaveUploadedFile_Responses> SaveUploadedFile(SaveUploadedFile_Parameters request, ServerCallContext context)
        {
            SaveUploadedFile_Responses response = new();

            try
            {
                string path = GetFilePath(request.UploadFileInfo.FileInfo.Path);

                var fileBytes = await _binaryUploadRepository.GetUploadedData(request.UploadFileInfo.FileInfo.FileBinaryUploadUuid.Value);

                File.WriteAllBytes(path, fileBytes);

                response.UploadResponse = new Sila2.Org.Silastandard.Boolean { Value = true };

                _binaryUploadRepository.UploadDataMap.TryRemove(Guid.Parse(request.UploadFileInfo.FileInfo.FileBinaryUploadUuid.Value), out IBinaryUploadRepository.UploadData _);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ErrorHandling.HandleException(ex));
                response.UploadResponse = new Sila2.Org.Silastandard.Boolean { Value = false };
            }

            return response;
        }

        public override async Task<PrepareFileDownload_Responses> PrepareFileDownload(PrepareFileDownload_Parameters request, ServerCallContext context)
        {
            PrepareFileDownload_Responses response = new();

            try
            {
                string path = GetFilePath(request.FilePath);

                var fileBytes = await File.ReadAllBytesAsync(path);
                var id = Guid.NewGuid();
                if (_binaryDownloadRepository.DownloadDataMap.TryAdd(id, fileBytes))
                {
                    response.DownloadFileUuid = new Sila2.Org.Silastandard.String { Value = id.ToString() };
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ErrorHandling.HandleException(ex));
            }

            return response;
        }

        public override Task<DownloadFile_Responses> DownloadFile(DownloadFile_Parameters request, ServerCallContext context)
        {
            DownloadFile_Responses response = new();

            try
            {
                response.DownloadFileResponse = new Sila2.Org.Silastandard.Binary { Value = ByteString.CopyFrom(_binaryDownloadRepository.DownloadDataMap[Guid.Parse(request.DownloadRepositoryUUID.Value)]) };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ErrorHandling.HandleException(ex));
            }

            return Task.FromResult(response);
        }

        private string GetFilePath(Sila2.Org.Silastandard.String filePath)
        {
            return (filePath.Value.Contains('/') || filePath.Value.Contains('\\')) ? filePath.Value : Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), filePath.Value);
        }
    }
}
