using NUnit.Framework;
using GrpcCoreMetaData = Grpc.Core.Metadata;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1;
using SiLA2.Temperature.Server.App;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using static Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService;
using static Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.TemperatureController;
using Sila2.Org.Silastandard;
using SiLA2.Utils.Extensions;
using static Sila2.Org.Silastandard.ExecutionInfo.Types;
using Grpc.Core;
using UnitTest.Utils;
using Grpc.Net.Client;
using Sila2.De.Chamundi.Util.Filetransfer.V1;
using SiLA2.Client;
using Microsoft.Extensions.Logging;
using Moq;
using System.IO;
using System.Linq;

namespace TemperatureControllerTests
{
    [TestFixture]
    public class ServerTests
    {
        private const string EXPECTED_SERVER_NAME = "SiLA2 Temperature Test Server";
        private const string EXPECTED_SERVER_TYPE = "SiLA2TemperatureServer";
        private const string EXPECTED_SERVER_UUID = "19767601-AEBF-4553-A8AC-75865BA8A072";

        private SiLAServiceClient _siLAServiceClient;
        private TemperatureControllerClient _temperatureControllerClient;
        private GrpcChannel _channel;

        private List<string> _expectedImplementedFeatures = new List<string>
                                                            {
                                                                { "org.silastandard/core/SiLAService/v1" },
                                                                { "org.silastandard/core/LockController/v2" },
                                                                { "org.silastandard/examples/TemperatureController/v1"},
                                                                { "de.chamundi/util/FileTransfer/v1"},
                                                                { "org.silastandard/core/AuthorizationService/v1"}
                                                            };

        [OneTimeSetUp]
        public void SetupOnce()
        {
            // System under Test
            var args = new string[] { };
            _channel = new TestServerFixture<Program>(args).GrpcChannel;
            _siLAServiceClient = new SiLAServiceClient(_channel);
            _temperatureControllerClient = new TemperatureControllerClient(_channel);
        }

        [Test]
        public async Task Should_Get_ServerNameAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerNameAsync(new Get_ServerName_Parameters());

            // Assert
            Assert.That(result.ServerName.Value, Is.EqualTo(EXPECTED_SERVER_NAME));
        }

        [Test]
        public void Should_Get_ServerName()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerName(new Get_ServerName_Parameters());

            // Assert
            Assert.That(result.ServerName.Value, Is.EqualTo(EXPECTED_SERVER_NAME));
        }

        [Test]
        public async Task Should_Get_ServerUUIDAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerUUIDAsync(new Get_ServerUUID_Parameters());

            // Assert
            Assert.That(result.ServerUUID.Value.ToUpper(), Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [Test]
        public void Should_Get_ServerUUID()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerUUID(new Get_ServerUUID_Parameters());

            // Assert
            Assert.That(result.ServerUUID.Value.ToUpper(), Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [Test]
        public async Task Should_Get_ServerTypeAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerTypeAsync(new Get_ServerType_Parameters());

            // Assert
            Assert.That(result.ServerType.Value, Is.EqualTo(EXPECTED_SERVER_TYPE));
        }

        [Test]
        public void Should_Get_ServerType()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerType(new Get_ServerType_Parameters());

            // Assert
            Assert.That(result.ServerType.Value, Is.EqualTo(EXPECTED_SERVER_TYPE));
        }

        [Test]
        public async Task Should_Get_ImplementedFeaturesAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ImplementedFeaturesAsync(new Get_ImplementedFeatures_Parameters());

            // Assert
            Assert.That(result.ImplementedFeatures.Count, Is.EqualTo(_expectedImplementedFeatures.Count));
            foreach (var feature in result.ImplementedFeatures)
            {
                Assert.That(_expectedImplementedFeatures.Contains(feature.Value));
            }
        }

        [Test]
        public void Should_Get_ImplementedFeatures()
        {
            // Act
            var result = _siLAServiceClient.Get_ImplementedFeatures(new Get_ImplementedFeatures_Parameters());

            // Assert
            Assert.That(result.ImplementedFeatures.Count, Is.EqualTo(_expectedImplementedFeatures.Count));
            foreach (var feature in result.ImplementedFeatures)
            {
                Assert.That(_expectedImplementedFeatures.Contains(feature.Value));
            }
        }

        [Test, Order(1)]
        public async Task Subscribe_CurrentTemperature_returns_same_value_of_observable_property_if_no_change_was_triggered()
        {
            // Arrange
            var cts = new CancellationTokenSource();
            const double DELTATEMPERATURE = 0.01;

            // System under Test
            var responseStream = _temperatureControllerClient.Subscribe_CurrentTemperature(new Subscribe_CurrentTemperature_Parameters(), GrpcCoreMetaData.Empty, null, cts.Token).ResponseStream;

            // Act
            await responseStream.MoveNext(cts.Token);
            var currentTemperature = responseStream.Current.CurrentTemperature.Value;

            // just get the value again, the temperature should be the same
            await responseStream.MoveNext(cts.Token);


            // Assert
            Assert.That(Math.Abs(currentTemperature - responseStream.Current.CurrentTemperature.Value) < DELTATEMPERATURE);

            // Teardown
            cts.Cancel();
        }

        [Test, Order(2)]
        public async Task Subscribe_CurrentTemperature_returns_different_values_of_observable_property_if_change_was_triggered()
        {
            // Arrange
            var cts = new CancellationTokenSource();
            const double TEMPERATURE = 80;
            var startTime = DateTime.Now;
            var duration = new TimeSpan(0, 0, 5); // check values and cancel after 5 seconds
            const double DELTATEMPERATURE = 0.01;

            // System under Test
            var responseStream = _temperatureControllerClient.Subscribe_CurrentTemperature(new Subscribe_CurrentTemperature_Parameters(), GrpcCoreMetaData.Empty, null, cts.Token).ResponseStream;
            await responseStream.MoveNext(cts.Token);
            var startTemperature = responseStream.Current.CurrentTemperature.Value;
            var currentTemperature = responseStream.Current.CurrentTemperature.Value;

            // Act
            _temperatureControllerClient.ControlTemperature(new ControlTemperature_Parameters { TargetTemperature = new Real { Value = TEMPERATURE.DegreeCelsius2Kelvin() } }, null, null, cts.Token);

            while (await responseStream.MoveNext() && DateTime.Now - startTime < duration)
            {
                currentTemperature = responseStream.Current.CurrentTemperature.Value;
            }

            // Assert
            Assert.That(Math.Abs(currentTemperature - responseStream.Current.CurrentTemperature.Value) > DELTATEMPERATURE);

            // Teardown
            cts.Cancel();
        }

        [Test]
        public async Task ControlTemperature_Response_Stream_Returns_CommandStatus()
        {
            // Arrange
            const double TEMPERATURE = 80;
            var cts = new CancellationTokenSource();
            cts.CancelAfter(5000);

            var commandConfirmation = _temperatureControllerClient.ControlTemperature(new ControlTemperature_Parameters
            { TargetTemperature = new Real { Value = TEMPERATURE.DegreeCelsius2Kelvin() } }, null, null, cts.Token);

            // System under Test
            using (var call = _temperatureControllerClient.ControlTemperature_Info(commandConfirmation.CommandExecutionUUID, null, null, cts.Token))
            {

                // Act
                await call.ResponseStream.MoveNext(cts.Token);

                //Assert
                Assert.That(call.ResponseStream.Current.CommandStatus == CommandStatus.Running);
            }
        }


        [Test]
        public async Task Should_Upload_File_To_Server_And_Download_It_Afterwards()
        {
            // Arrange
            const string PARAMETER_IDENTIFIER = "de.chamundi/util/FileTransfer/v1/Command/UploadFile/Parameter/FileBytes";
            const string SERVER_CRT = "server.crt";
            const string FILE_NAME_ON_SERVER = $"{SERVER_CRT}_tmp";
            const int CHUNK_SIZE = 100;

            var logger = new Mock<ILogger<BinaryClientService>>();
            var binaryUploadClient = new BinaryClientService(new BinaryUpload.BinaryUploadClient(_channel), new BinaryDownload.BinaryDownloadClient(_channel), logger.Object);

            var fileInputBytes = File.ReadAllBytes(SERVER_CRT);
            var cmdUuid = await binaryUploadClient.UploadBinary(fileInputBytes, CHUNK_SIZE, PARAMETER_IDENTIFIER);

            // System under Test
            var fileTransferClient = new FileTransfer.FileTransferClient(_channel);

            // Act
            SaveUploadedFile_Parameters requestParameter = new SaveUploadedFile_Parameters
            {
                UploadFileInfo = new DataType_FileInfo
                {
                    FileInfo = new DataType_FileInfo.Types.FileInfo_Struct
                    {
                        FileBinaryUploadUuid = new Sila2.Org.Silastandard.String { Value = cmdUuid },
                        Path = new Sila2.Org.Silastandard.String { Value = FILE_NAME_ON_SERVER }
                    }
                }
            };
            var result = await fileTransferClient.SaveUploadedFileAsync(requestParameter);

            var prepareDownloadResponse = fileTransferClient.PrepareFileDownload(new PrepareFileDownload_Parameters { FilePath = new Sila2.Org.Silastandard.String { Value = FILE_NAME_ON_SERVER } });

            var downloadBytesResponse = await fileTransferClient.DownloadFileAsync(new DownloadFile_Parameters { DownloadRepositoryUUID = prepareDownloadResponse.DownloadFileUuid });

            // Assert
            Assert.That(result.UploadResponse.Value, result.UploadResponse.Value ? "Upload succeeded:" : "Upload failed!");
            Assert.That(fileInputBytes.SequenceEqual(downloadBytesResponse.DownloadFileResponse.Value.ToByteArray()));
        }
    }
}