﻿using Sila2.Org.Silastandard.Core.Authenticationservice.V1;
using SiLA2.Frontend.Razor.Services.UserManagement;
using SiLA2.Server.Services;
using SiLA2.Utils;
using SiLA2.Utils.Network;

namespace Auth.Server.App.Services
{
    public class AuthenticationInspector : IAuthenticationInspector
    {
        private readonly IUserService _userService;
        private readonly IServerConfig _serverConfig;
        private readonly ILogger<IAuthenticationInspector> _logger;

        public AuthenticationInspector(IUserService userService, IServerConfig serverConfig, ILogger<IAuthenticationInspector> logger)
        {
            _userService = userService;
            _serverConfig = serverConfig;
            _logger = logger;
        }

        public Task<bool> IsAuthenticated(Login_Parameters login)
        {
            bool result = false;
            try
            {
                var loginUser = _userService.GetUser(login.UserIdentification.Value);
                var pwdHash = new PasswordHashService();
                result = pwdHash.Verify(loginUser.Password, login.Password.Value);
                if (result)
                {
                    _logger.LogDebug("Password validated");

                    result = Guid.Parse(login.RequestedServer.Value) == _serverConfig.Uuid;
                    if (result)
                    {
                        _logger.LogDebug("SiLA2 Server validated");
                    }
                    else
                    {
                        _logger.LogWarning($"Wrong server request ({login.RequestedServer.Value}) !");
                    }
                }
                else
                    _logger.LogWarning("Password invalid !");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, null);
            }

            return Task.FromResult(result);
        }
    }
}
