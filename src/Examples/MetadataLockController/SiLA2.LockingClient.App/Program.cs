﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using SiLA2.Client;
using SiLA2.Utils.Extensions;
using SiLA2.Utils.Network;

namespace SiLA2.LockingClient.App
{
    internal class Program
    {
        private static IConfigurationRoot _configuration;

        static async Task Main(string[] args)
        {
            try
            {
                var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                _configuration = configBuilder.Build();

                var clientSetup = new Configurator(_configuration, args);

                Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(_configuration)
                    .CreateLogger();

                clientSetup.Container.AddLogging(x => {
                    x.ClearProviders();
                    x.AddSerilog(dispose: true);
                });
                clientSetup.UpdateServiceProvider();

                var logger = clientSetup.ServiceProvider.GetRequiredService<ILogger<Program>>();

                logger.LogInformation("Starting Server Discovery...");

                var serverMap = await clientSetup.SearchForServers();

                GrpcChannel channel;
                const string serverType = "SiLA2MetadataLockControllerServer";
                var server = serverMap.Values.FirstOrDefault(x => x.ServerType == serverType);
                if (server != null)
                {
                    logger.LogInformation("Found Server");
                    logger.LogInformation(server.ServerInfo);
                    logger.LogInformation($"Connecting to {server}");
                    channel = await clientSetup.GetChannel(server.Address, server.Port, acceptAnyServerCertificate: false, server.SilaCA.GetCaFromFormattedCa());
                }
                else
                {
                    var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
                    logger.LogInformation($"No connection automatically discovered. Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
                    channel = await clientSetup.GetChannel(acceptAnyServerCertificate: true);
                }

                logger.LogInformation("Trying to setup Client ...");
                var clientImpl = new Client(channel, clientSetup.ServiceProvider.GetService<ILoggerFactory>());
                logger.LogInformation("Client Setup successful for following Clients :");
                foreach (var client in clientImpl.Clients)
                {
                    logger.LogInformation($"{client.GetType().FullName}");
                }

                // command Get_FCPAffectedByMetadata_LockIdentifier
                var identifiers = clientImpl.GetLockAffectedIdentifiers;
                logger.LogInformation("\nIdentifiers affected by locking:");
                foreach (var i in identifiers)
                {
                    logger.LogInformation($" - '{i}'");
                }

                // check log state
                logger.LogInformation("\n--> request: \"IsServerLocked()\" ...");
                CheckResponse(clientImpl.IsServerLocked);

                // property CurrentYear
                logger.LogInformation("\n--> request: \"Get_StartYear()\" ...");
                logger.LogInformation($"<-- response: \"{clientImpl.GetStartYear()}\"");

                // command SayHello (without required metadata)
                const string param = "SiLA user";
                logger.LogInformation($"\n--> request: \"SayHello(Name='{param}')\" (without metadata) ...");
                CheckResponse(clientImpl.SayHello(param));

                await Task.Delay(1000);

                // command SayHello (with required metadata but empty content)
                logger.LogInformation($"\n--> request: \"SayHello(Name='{param}')\" (with LockIdentifier metadata but empty value) ...");
                CheckResponse(clientImpl.SayHelloWithLockIdentifier(param, string.Empty));

                await Task.Delay(1000);

                // lock server
                var lockIdentifier = "MY_LOCK_ID";
                var timeout = 0;
                logger.LogInformation($"\n--> request: \"LockServer(LockIdentifier='{lockIdentifier}', Timeout={timeout})\" ...");
                clientImpl.LockServer(lockIdentifier, timeout);

                // check log state
                logger.LogInformation("\n--> request: \"IsServerLocked()\" ...");
                CheckResponse(clientImpl.IsServerLocked);

                // try to lock server (already in lock state)
                logger.LogInformation($"\n--> request: \"LockServer(LockIdentifier='{lockIdentifier}', Timeout={timeout})\" ...");
                clientImpl.LockServer(lockIdentifier, timeout);

                await Task.Delay(1000);

                // try to call SiLAService property with metadata (not allowed)
                logger.LogInformation($"\n--> request: \"Get_ImplementedFeatures()\" (with metadata lock identifier  '{lockIdentifier}') ...");
                clientImpl.CallSiLAServicePropertyWithMetadata(lockIdentifier);

                await Task.Delay(1000);

                // property CurrentYear
                logger.LogInformation("\n--> request: \"Get_StartYear()\" (not locked) ...");
                logger.LogInformation($"<-- response: \"{clientImpl.GetStartYear()}\"");

                await Task.Delay(1000);

                // command SayHello (with wrong metadata key)
                logger.LogInformation($"\n--> request: \"SayHello(Name='{param}')\" (with wrong metadata key) ...");
                CheckResponse(clientImpl.SayHelloWithWrongLockIdentifierKey(param, lockIdentifier));

                await Task.Delay(1000);

                // command SayHello (with metadata containing no Metadata_LockIdentifier but a SiLA Integer type)
                logger.LogInformation($"\n--> request: \"SayHello(Name='{param}')\" (with Integer SiLA type in metadata content) ...");
                CheckResponse(clientImpl.SayHelloWithIntegerLockIdentifier(param, 42));

                await Task.Delay(1000);

                // command SayHello with lock identifier format - not a gRPC message
                logger.LogInformation($"\n--> request: \"SayHello(Name='{param}')\" (with wrong metadata content format) ...");
                CheckResponse(clientImpl.SayHelloWithWrongFormatLockIdentifierContent(param, lockIdentifier));

                await Task.Delay(1000);

                // command SayHello (with metadata containing invalid lock identifier)
                logger.LogInformation($"\n--> request: \"SayHello(Name='{param}')\" (with metadata lock identifier 'something_else') ...");
                CheckResponse(clientImpl.SayHelloWithLockIdentifier(param, "something_else"));

                await Task.Delay(1000);

                // command SayHello (with required metadata)
                logger.LogInformation($"\n--> request: \"SayHello(Name='{param}')\" (with metadata lock identifier '{lockIdentifier}') ...");
                CheckResponse(clientImpl.SayHelloWithLockIdentifier(param, lockIdentifier));

                // try unlock server without valid lock identifier
                logger.LogInformation("\n--> request: \"UnlockServer(LockIdentifier='just_guessing')\" ...");
                clientImpl.UnlockServer("just_guessing");

                await Task.Delay(1000);

                // unlock server with valid lock identifier
                logger.LogInformation($"\n--> request: \"UnlockServer(LockIdentifier='{lockIdentifier}')\" ...");
                clientImpl.UnlockServer(lockIdentifier);

                // check log state 
                logger.LogInformation("\n--> request: \"IsServerLocked()\" ...");
                CheckResponse(clientImpl.IsServerLocked);

                // try to unlock server (not locked)
                logger.LogInformation($"\n--> request: \"UnlockServer(LockIdentifier='{lockIdentifier}')\" ...");
                clientImpl.UnlockServer(lockIdentifier);

                await Task.Delay(1000);

                // lock server with a timeout
                lockIdentifier = "MY_NEW_LOCK_ID";
                timeout = 5;
                logger.LogInformation($"\n--> request: \"LockServer(LockIdentifier='{lockIdentifier}', Timeout={timeout}s)\" ...");
                clientImpl.LockServer(lockIdentifier, timeout);

                logger.LogInformation("Waiting for 4 seconds ...");
                Thread.Sleep(4000);

                // command SayHello (with required metadata) to reset the lock timeout timer
                logger.LogInformation($"\n--> request: \"SayHello(Name='{param}')\" (with metadata lock identifier  '{lockIdentifier}') -> resets the lock timeout");
                CheckResponse(clientImpl.SayHelloWithLockIdentifier(param, lockIdentifier));

                logger.LogInformation("Waiting for 4 seconds ...");

                // check log state 
                logger.LogInformation("\n--> request: \"IsServerLocked()\" ...");
                CheckResponse(clientImpl.IsServerLocked);

                logger.LogInformation("Waiting for 5 seconds to elapse the lock timeout...");
                Thread.Sleep(5000);

                // check log state 
                logger.LogInformation("\n--> request: \"IsServerLocked()\" ...");
                CheckResponse(clientImpl.IsServerLocked);

                await Task.Delay(1000);
                logger.LogInformation("Shutting down connection...");

                channel.ShutdownAsync().Wait();
                Console.WriteLine();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                Console.WriteLine("Client Error: {0}", e);
            }

            Console.ReadKey();
        }

        private static void CheckResponse(string response)
        {
            if (string.IsNullOrEmpty(response)) { return; }

            Console.WriteLine($"<-- response: \"{response}\"");
        }

        private static void CheckResponse(bool? response)
        {
            if (response != null)
            {
                Console.WriteLine($"<-- response: \"{(bool)response}\"");
            }
        }
    }
}
