﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using SiLA2.Client;
using SiLA2.Utils.Extensions;
using SiLA2.Utils.Network;
using Framework = Sila2.Org.Silastandard;

namespace SiLA2.DataTypeProvider.Client.App
{
    internal class Program
    {
        private static IConfigurationRoot _configuration;

        static async Task Main(string[] args)
        {
            try
            {
                var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                _configuration = configBuilder.Build();

                var clientSetup = new Configurator(_configuration, args);

                Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(_configuration)
                    .CreateLogger();

                clientSetup.Container.AddLogging(x => { 
                    x.ClearProviders();
                    x.AddSerilog(dispose: true);
                });
                clientSetup.UpdateServiceProvider();

                var logger = clientSetup.ServiceProvider.GetRequiredService<ILogger<Program>>();

                logger.LogInformation("Starting Server Discovery...");

                var serverMap = await clientSetup.SearchForServers();

                GrpcChannel channel;
                var serverType = "SiLA2DataTypeProviderServer";
                var server = serverMap.Values.FirstOrDefault(x => x.ServerType == serverType);
                if (server != null)
                {
                    logger.LogInformation("Found Server");
                    logger.LogInformation(server.ServerInfo);
                    logger.LogInformation($"Connecting to {server}");
                    channel = await clientSetup.GetChannel(server.Address, server.Port, acceptAnyServerCertificate: false, server.SilaCA.GetCaFromFormattedCa());
                }
                else
                {
                    var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
                    logger.LogWarning($"No connection automatically discovered. Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
                    channel = await clientSetup.GetChannel(acceptAnyServerCertificate: true);
                }

                logger.LogInformation("Trying to setup Client ...");
                var clientImpl = new Client(channel, clientSetup.ServiceProvider.GetService<ILoggerFactory>());
                logger.LogInformation("Client Setup successful for following Clients :");
                foreach (var client in clientImpl.Clients)
                {
                    logger.LogInformation($"{client.GetType().FullName}");
                }

                logger.LogInformation("--- Binary transfer ---");

                // property BinaryValueDirectly
                logger.LogInformation("Calling 'Get_BinaryValueDirectly()' ...");
                logger.LogInformation($"Response: '{clientImpl.GetBinaryValueDirectly()}'");

                // property BinaryValueDownload
                logger.LogInformation("Calling 'Get_BinaryValueDownload()' ...");
                await clientImpl.GetBinaryValueDownload();

                // command SetBinaryValue
                logger.LogInformation("Calling 'SetBinaryValue' with sending the value directly ...");
                logger.LogInformation(clientImpl.SetStringAsDirectBinaryValue("SiLA2_Test_String_Value"));

                logger.LogInformation("Calling 'SetBinaryValue' with binary upload ...");
                await clientImpl.SetBinaryValueUpload(System.Text.Encoding.UTF8.GetBytes("A_somewhat_longer_test_string_to_demonstrate_the_binary_upload"), 5);
                logger.LogInformation($"Response: '{System.Text.Encoding.UTF8.GetString(clientImpl.ResponseData)}'");

                // generate random sequence of characters
                const int dataSize = 10 * 1024 * 1024; // 10 MB
                var sourceData = new byte[dataSize];
                var rnd = new Random((int)DateTime.Now.Ticks);
                rnd.NextBytes(sourceData);

                // command SetBinaryValue with large binary data
                const int chunkSize = 65536; //Recommended chunk size for large messages -> 16384 to 65536 Bytes
                logger.LogInformation($"Calling 'SetBinaryValue' with a random sequence of {dataSize} bytes and chunk size of {chunkSize} ...");
                await clientImpl.SetBinaryValueUpload(sourceData, chunkSize);
                if (sourceData.SequenceEqual(clientImpl.ResponseData))
                {
                    logger.LogInformation($"{DateTime.Now} Received response data equals sent data");
                }
                else
                {
                    logger.LogError("ERROR: Received response data differs from sent data!");
                }

                logger.LogInformation("--- Any type values ---");
                clientImpl.GetAnyTypeValues();

                // setting Any type values
                const string stringValue = "My_Any_type_string_value";
                logger.LogInformation($"Set string value '{stringValue}'");
                logger.LogInformation(clientImpl.SetAnyTypeStringValue(stringValue));

                const int integerValue = 123456789;
                logger.LogInformation($"Set integer value {integerValue}");
                logger.LogInformation(clientImpl.SetAnyTypeIntegerValue(integerValue));

                const double realValue = 9.87654321;
                logger.LogInformation($"Set real value {realValue}");
                logger.LogInformation(clientImpl.SetAnyTypeRealValue(realValue));

                const bool booleanValue = true;
                logger.LogInformation($"Set boolean value {booleanValue}");
                logger.LogInformation(clientImpl.SetAnyTypeBooleanValue(booleanValue));

                const string binaryTypeStringValue = "My_Any_type_binary_value_string_content";
                logger.LogInformation($"Set binary type value with embedded string content '{binaryTypeStringValue}'");
                logger.LogInformation(clientImpl.SetAnyTypeBinaryValue(binaryTypeStringValue));

                var dateTimeValue = new DateTime(2021, 7, 29, 22, 33, 44);
                var timeZone = new TimeSpan(2, 30, 0);
                logger.LogInformation($"Set date value {dateTimeValue.Date:d}, time zone {timeZone:hh\\:mm}");
                logger.LogInformation(clientImpl.SetAnyTypeDateValue(new Framework.Date
                {
                    Day = (uint)dateTimeValue.Day,
                    Month = (uint)dateTimeValue.Month,
                    Year = (uint)dateTimeValue.Year,
                    Timezone = new Framework.Timezone
                    {
                        Hours = timeZone.Hours,
                        Minutes = (uint)timeZone.Minutes
                    }
                }));

                logger.LogInformation($"Set time value {dateTimeValue.TimeOfDay}, time zone {timeZone:hh\\:mm}");
                logger.LogInformation(clientImpl.SetAnyTypeTimeValue(new Framework.Time
                {
                    Second = (uint)dateTimeValue.Second,
                    Minute = (uint)dateTimeValue.Minute,
                    Hour = (uint)dateTimeValue.Hour,
                    Timezone = new Framework.Timezone
                    {
                        Hours = timeZone.Hours,
                        Minutes = (uint)timeZone.Minutes
                    }
                }));

                logger.LogInformation($"Set time stamp value {dateTimeValue}, time zone {timeZone:hh\\:mm}");
                logger.LogInformation(clientImpl.SetAnyTypeTimestampValue(new Framework.Timestamp
                {
                    Second = (uint)dateTimeValue.Second,
                    Minute = (uint)dateTimeValue.Minute,
                    Hour = (uint)dateTimeValue.Hour,
                    Day = (uint)dateTimeValue.Day,
                    Month = (uint)dateTimeValue.Month,
                    Year = (uint)dateTimeValue.Year,
                    Timezone = new Framework.Timezone
                    {
                        Hours = timeZone.Hours,
                        Minutes = (uint)timeZone.Minutes
                    }
                }));

                await Task.Delay(1000);
                logger.LogInformation("Shutting down connection...");

                await channel.ShutdownAsync();
                Console.WriteLine();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                Console.WriteLine("Client Error: {0}", e);
            }

            Console.ReadKey();
        }
    }
}
