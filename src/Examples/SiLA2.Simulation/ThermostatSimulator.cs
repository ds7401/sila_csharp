using Microsoft.Extensions.Logging;
using SiLA2.Utils.Extensions;
using System;
using System.Threading;

namespace SiLA2.Simulation
{
    public class ThermostatSimulator : IThermostatSimulator
    {
        public const double KELVIN_PER_SECONDS = 4; // [K/s] Update ramp
        private const double KELVIN_ACCURACY = 1; // [K] When control is assumed to be finished
        private const double UPDATE_INTERVAL = 1; // [s] Interval temperature is updated
        private const double KELVIN_RAMP = KELVIN_PER_SECONDS * UPDATE_INTERVAL; // [Kelvin / step]
        private readonly object _lock = new object();
        private readonly ILogger<ThermostatSimulator> _logger;

        public double CurrentTemperature
        {
            get => GetCurrentTemperature();
        }

        public ThermostatSimulator(ILogger<ThermostatSimulator> logger)
        {
            _logger = logger;
        }

        public double TargetTemperature { get; private set; }
        private double _currentTemperature = 20.0.DegreeCelsius2Kelvin();

        public void SetTargetTemperature(double temperature, CancellationToken cancellationToken)
        {
            TargetTemperature = temperature;
            _logger.LogInformation($"Target Temperature: {TargetTemperature}");
            _logger.LogInformation($"Current Temperature: {_currentTemperature}");
            var temperatureSet = false;
            while (!(temperatureSet || cancellationToken.IsCancellationRequested))
            {
                var startTime = DateTime.Now;
                // Linear Increase to target temperature until accuracy reached
                var temperatureDifference = TargetTemperature - _currentTemperature;
                var absoluteTemperatureDifference = Math.Abs(temperatureDifference);

                if (absoluteTemperatureDifference >= KELVIN_ACCURACY)
                {
                    if (KELVIN_RAMP > absoluteTemperatureDifference)
                    {
                        _currentTemperature = TargetTemperature;
                    }
                    else
                    {
                        lock (_lock)
                        {
                            _currentTemperature += Math.Sign(temperatureDifference) * KELVIN_RAMP;
                        }
                    }
                }
                else
                {
                    temperatureSet = true;
                }
                // Best effort to keep the update interval
                var remainingTime = TimeSpan.FromSeconds(UPDATE_INTERVAL) - (DateTime.Now - startTime);
                if (remainingTime > TimeSpan.Zero)
                    Thread.Sleep((int)remainingTime.TotalMilliseconds);
            }

            _logger.LogInformation($"End of temperature change agent");
            _logger.LogInformation($"Current Temperature: {_currentTemperature}");
        }

        private double GetCurrentTemperature()
        {
            lock (_lock)
            {
                return _currentTemperature;
            }
        }
    }

}