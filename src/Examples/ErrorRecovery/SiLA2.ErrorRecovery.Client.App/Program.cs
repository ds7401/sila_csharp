﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using SiLA2.Client;
using SiLA2.Utils.Extensions;
using SiLA2.Utils.Network;

namespace SiLA2.ErrorRecovery.Client.App
{
    internal class Program
    {
        private static IConfigurationRoot _configuration;

        static async Task Main(string[] args)
        {
            try
            {
                var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                _configuration = configBuilder.Build();

                var clientSetup = new Configurator(_configuration, args);
                Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(_configuration)
                    .CreateLogger();

                clientSetup.Container.AddLogging(x => {
                    x.ClearProviders();
                    x.AddSerilog(dispose: true);
                });
                clientSetup.UpdateServiceProvider();

                var logger = clientSetup.ServiceProvider.GetRequiredService<ILogger<Program>>();

                logger.LogInformation("Starting Server Discovery...");

                var serverMap = await clientSetup.SearchForServers();

                GrpcChannel channel;
                var serverType = "SiLA2ErrorRecoveryServer";
                var server = serverMap.Values.FirstOrDefault(x => x.ServerType == serverType);
                if (server != null)
                {
                    logger.LogInformation("Found Server");
                    logger.LogInformation(server.ServerInfo);
                    logger.LogInformation($"Connecting to {server}");
                    channel = await clientSetup.GetChannel(server.Address, server.Port, acceptAnyServerCertificate: false, server.SilaCA.GetCaFromFormattedCa());
                }
                else
                {
                    var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
                    logger.LogInformation($"No connection automatically discovered. Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
                    channel = await clientSetup.GetChannel(acceptAnyServerCertificate: true);
                }

                logger.LogInformation("Trying to setup Client ...");

                var clientImpl = new Client(channel, clientSetup.ServiceProvider.GetService<ILoggerFactory>());
                logger.LogInformation("Client Setup successful for following Clients :");
                foreach (var client in clientImpl.Clients)
                {
                    logger.LogInformation($"{client.GetType().FullName}");
                }

                logger.LogInformation("\n-----------------------------------------------------------------------------------------------------------------------");
                logger.LogInformation("Calling \"Subscribe_RecoverableErrors\" ...");
                await clientImpl.StartSubscribingErrors();

                // raising an error after 3s without automatic selection
                int preErrorDuration = 3;
                int postErrorDuration = 5;
                int automaticSelectionTimeout = 0;
                clientImpl.HandleErrorManually = true;
                logger.LogInformation("\n-----------------------------------------------------------------------------------------------------------------------");
                logger.LogInformation($"Calling \"RaiseRecoverableError after {preErrorDuration}s (automaticSelectionTimeout = {automaticSelectionTimeout}s , duration after error handling = {postErrorDuration}s)\" ...");
                await clientImpl.RaiseRecoverableError("TestError_1", "Test error to be handled manually", preErrorDuration, postErrorDuration, automaticSelectionTimeout);

                await Task.Delay(1000);

                // raising an error after 3s with automatic execution after 3s
                automaticSelectionTimeout = 3;
                clientImpl.HandleErrorManually = false;
                logger.LogInformation("\n-----------------------------------------------------------------------------------------------------------------------");
                logger.LogInformation($"Calling \"RaiseRecoverableError after {preErrorDuration}s (automaticSelectionTimeout = {automaticSelectionTimeout}s , duration after error handling = {postErrorDuration}s)\" ...");
                await clientImpl.RaiseRecoverableError("TestError_2", $"Test error with automatic handling after {automaticSelectionTimeout}s", preErrorDuration, postErrorDuration, automaticSelectionTimeout);

                await Task.Delay(1000);

                // set error handling timeout to 8s and raise an error without automatic error handling
                int timeout = 8;
                logger.LogInformation("\n-----------------------------------------------------------------------------------------------------------------------");
                logger.LogInformation($"Calling \"SetErrorHandlingTimeout({timeout}s)\" ...");
                clientImpl.SetErrorHandlingTimeout(timeout);

                await Task.Delay(1000);

                automaticSelectionTimeout = 0;
                logger.LogInformation("\n-----------------------------------------------------------------------------------------------------------------------");
                logger.LogInformation($"Calling \"RaiseRecoverableError after {preErrorDuration}s (automaticSelectionTimeout = {automaticSelectionTimeout}s , duration after error handling = {postErrorDuration}s)\" ...");
                await clientImpl.RaiseRecoverableError("TestError_3", "Test error without manual or automatic handling", preErrorDuration, postErrorDuration, automaticSelectionTimeout);

                await Task.Delay(1000);

                // canceling error subscription
                logger.LogInformation("\n-----------------------------------------------------------------------------------------------------------------------");
                logger.LogInformation("Unsubscribing \"RecoverableErrors\" property ...");
                clientImpl.StopSubscribingErrors();

                // reset error handling timeout to 0s and raise an error without subscription and automatic error handling
                timeout = 0;
                logger.LogInformation("\n-----------------------------------------------------------------------------------------------------------------------");
                logger.LogInformation($"Calling \"SetErrorHandlingTimeout({timeout}s)\" ...");
                clientImpl.SetErrorHandlingTimeout(timeout);

                logger.LogInformation("\n-----------------------------------------------------------------------------------------------------------------------");
                logger.LogInformation($"Calling \"RaiseRecoverableError after {preErrorDuration}s (automaticExecutionTimeout = {automaticSelectionTimeout}s , duration after error handling = {postErrorDuration}s)\" without subscribing to the \"RecoverableErrors\" property ...");
                await clientImpl.RaiseRecoverableError("TestError_4", "Test error without RecoverableErrors subscription", preErrorDuration, postErrorDuration, automaticSelectionTimeout);

                await Task.Delay(1000);
                logger.LogInformation("Shutting down connection...");

                channel.ShutdownAsync().Wait();
                Console.WriteLine();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                Console.WriteLine("Client Error: {0}", e);
            }

            Console.ReadKey();
        }
    }
}
