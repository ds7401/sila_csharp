﻿using System.Text;
using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;
using RecoverableErrorProvider = Sila2.De.Equicon.Examples.Recoverableerrorprovider.V2;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.ErrorRecovery.Client.App
{
    using ErrorRecovery = SiLAFramework.Core.Errorrecoveryservice.V2;

    public class Client
    {
        #region Instances of the gRPC clients for the implemented features (generated by the proto compiler)

        private readonly RecoverableErrorProvider.RecoverableErrorProvider.RecoverableErrorProviderClient _recoverableErrorProviderClient;
        private readonly ErrorRecovery.ErrorRecoveryService.ErrorRecoveryServiceClient _errorRecoveryClient;

        #endregion

        private readonly ILogger<Client> _logger;

        public IList<ClientBase> Clients { get; } = new List<ClientBase>();

        private bool _errorSubscriptionRunning;
        private CancellationTokenSource _cancellationToken;
        private CancellationTokenSource _moveNextCancellationToken;

        public Client(GrpcChannel channel, ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<Client>();
            _recoverableErrorProviderClient = new RecoverableErrorProvider.RecoverableErrorProvider.RecoverableErrorProviderClient(channel);
            Clients.Add(_recoverableErrorProviderClient);
            _errorRecoveryClient = new ErrorRecovery.ErrorRecoveryService.ErrorRecoveryServiceClient(channel);
            Clients.Add(_errorRecoveryClient);
        }

        public bool HandleErrorManually { private get; set; }

        #region ErrorRecoveryService

        public void SetErrorHandlingTimeout(int timeout)
        {
            _errorRecoveryClient.SetErrorHandlingTimeout(new ErrorRecovery.SetErrorHandlingTimeout_Parameters { ErrorHandlingTimeout = new ErrorRecovery.DataType_Timeout { Timeout = new Integer { Value = timeout } } });
        }

        public async Task StartSubscribingErrors()
        {
            try
            {
                Task.Run(async () =>
                {
                    _cancellationToken = new CancellationTokenSource();
                    _moveNextCancellationToken = new CancellationTokenSource();
                    var responseStream = _errorRecoveryClient.Subscribe_RecoverableErrors(new ErrorRecovery.Subscribe_RecoverableErrors_Parameters(), Grpc.Core.Metadata.Empty, null, _cancellationToken.Token).ResponseStream;
                    while (!_moveNextCancellationToken.IsCancellationRequested && await responseStream.MoveNext(_moveNextCancellationToken.Token))
                    {
                        if (!_errorSubscriptionRunning)
                        {
                            _errorSubscriptionRunning = true;
                            _logger.LogInformation("Error subscription started");
                        }

                        ConsoleOutput(GetCurrentErrorsString(responseStream.Current.RecoverableErrors), true, ConsoleColor.DarkYellow);

                        // handle the error by reading the user input or wait for automatically handling (async)
                        DoErrorHandling(responseStream.Current);
                    }
                });

                while (!_errorSubscriptionRunning)
                {
                    await Task.Delay(200);
                }
            }
            catch (RpcException e)
            {
                if (e.Status.StatusCode == StatusCode.Cancelled)
                {
                    // CANCELED exception is not handled here because cancellation has been required
                    _logger.LogInformation("Error subscription canceled");
                    return;
                }
                _logger.LogError(ErrorHandling.HandleException(e));
            }
        }

        public void StopSubscribingErrors()
        {
            try
            {
                if (_errorSubscriptionRunning)
                {
                    _errorSubscriptionRunning = false;
                    if (_moveNextCancellationToken != null)
                    {
                        _logger.LogDebug("Canceling error subscription (MoveNext) ...");
                        _moveNextCancellationToken.Cancel();
                        _moveNextCancellationToken = null;


                        if (_cancellationToken != null)
                        {
                            _logger.LogDebug("Canceling error subscription (Stream) ...");
                            _cancellationToken.Cancel();
                            _cancellationToken = null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Exception while canceling error subscription: " + e.Message);
            }
        }

        #endregion

        #region RecoverableErrorProvider commands

        public async Task RaiseRecoverableError(string errorIdentifier, string errorMessage, int durationTillError, int durationAfterError, int automaticSelectionTimeout)
        {
            var methodName = new System.Diagnostics.StackTrace().GetFrame(2)?.GetMethod()?.Name ?? "unknown";

            try
            {
                // start command
                var cmdId = _recoverableErrorProviderClient.RaiseRecoverableError(new RecoverableErrorProvider.RaiseRecoverableError_Parameters
                {
                    ErrorIdentifier = new SiLAFramework.String { Value = errorIdentifier },
                    ErrorMessage = new SiLAFramework.String { Value = errorMessage },
                    DurationTillError = new Integer { Value = durationTillError },
                    DurationAfterError = new Integer { Value = durationAfterError },
                    AutomaticSelectionTimeout = new Integer { Value = automaticSelectionTimeout }
                }).CommandExecutionUUID;
                _logger.LogDebug($"{methodName} command started (id = {cmdId.Value}) ...");

                // wait for command execution to finish
                using (var call = _recoverableErrorProviderClient.RaiseRecoverableError_Info(cmdId))
                {
                    var responseStream = call.ResponseStream;
                    while (await responseStream.MoveNext())
                    {
                        var currentExecutionInfo = responseStream.Current;
                        if (currentExecutionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully || currentExecutionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError)
                        {
                            break;
                        }
                    }
                }

                // get and log response
                var response = _recoverableErrorProviderClient.RaiseRecoverableError_Result(cmdId);
                _logger.LogInformation("Response: " + response.ErrorHandlingResult.Value);
            }
            catch (RpcException e)
            {
                _logger.LogError(ErrorHandling.HandleException(e));
            }
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// Handles the first error of the RecoverableErrors property value by reading the user input (continuation option) from the console.
        /// If no recoverable error is contained, cancel a potentially pending  reading request in case the error has been handled automatically meanwhile.
        /// </summary>
        /// <param name="response">The current value of the RecoverableErrors property</param>
        private void DoErrorHandling(ErrorRecovery.Subscribe_RecoverableErrors_Responses response)
        {
            if (response.RecoverableErrors.Count == 0)
            {
                // no errors to be handled
                return;
            }

            // handle first error
            var recoverableError = response.RecoverableErrors[0].RecoverableError;

            // write error text
            ConsoleOutput($"Recoverable error \"{recoverableError.ErrorMessage.Value}\" in command {recoverableError.CommandIdentifier.Value} (id={recoverableError.CommandExecutionUUID.Value})", true, ConsoleColor.Red);
            var s = new StringBuilder("  available continuation options:\n");
            var number = 0;
            foreach (var co in recoverableError.ContinuationOptions)
            {
                s.AppendLine($"  {++number} - [{co.ContinuationOption.Identifier.Value}] {(recoverableError.DefaultOption.Value == co.ContinuationOption.Identifier.Value && recoverableError.AutomaticSelectionTimeout.Timeout.Value > 0 ? $"(DEFAULT, {recoverableError.AutomaticSelectionTimeout.Timeout.Value} s)" : string.Empty)} '{co.ContinuationOption.Description.Value}'");
            }

            var selectedOption = string.Empty;
            if (!HandleErrorManually)
            {
                ConsoleOutput(s + "Waiting instead of selecting a continuation option for demonstrating automatic handling ...", true, ConsoleColor.Cyan);

                if (recoverableError.AutomaticSelectionTimeout.Timeout.Value > 0 &&
                    recoverableError.ContinuationOptions.Select(co => co.ContinuationOption.Identifier.Value).ToList().Contains(recoverableError.DefaultOption.Value))
                {
                    // wait for automatic selection timeout
                    Thread.Sleep(new TimeSpan(0, 0, (int)recoverableError.AutomaticSelectionTimeout.Timeout.Value));

                    // handle error by selecting the default option
                    selectedOption = recoverableError.DefaultOption.Value;
                }
                else
                {
                    // wait for error handling timeout
                    return;
                }
            }
            else
            {
                // read user input
                do
                {
                    s.Append("Select continuation option number: ");
                    ConsoleOutput(s.ToString(), false, ConsoleColor.Cyan);

                    // read user input from console
                    var key = Console.ReadKey();
                    Console.WriteLine("");

                    if (!int.TryParse(key.KeyChar.ToString(), out var selectedOptionNumber))
                    {
                        ConsoleOutput("Entered key is not a number!", true, ConsoleColor.Red);
                        continue;
                    }

                    if (selectedOptionNumber < 1 || selectedOptionNumber > recoverableError.ContinuationOptions.Count)
                    {
                        ConsoleOutput($"Invalid option number! (must be between 1 and {recoverableError.ContinuationOptions.Count})", true, ConsoleColor.Red);
                        continue;
                    }

                    selectedOption = recoverableError.ContinuationOptions[selectedOptionNumber - 1].ContinuationOption.Identifier.Value;
                } while (string.IsNullOrEmpty(selectedOption));
            }

            // handle error
            _logger.LogInformation($"Sending continuation option '{selectedOption}' ...");
            var handleErrorResponse = _errorRecoveryClient.ExecuteContinuationOption(
                new ErrorRecovery.ExecuteContinuationOption_Parameters
                {
                    CommandExecutionUUID = recoverableError.CommandExecutionUUID,
                    ContinuationOption = new SiLAFramework.String { Value = selectedOption }
                });
        }

        private static string GetCurrentErrorsString(Google.Protobuf.Collections.RepeatedField<ErrorRecovery.DataType_RecoverableError> errors)
        {
            var s = new StringBuilder($"\n[error subscription] {(errors.Count == 0 ? "no" : errors.Count.ToString())} unhandled recoverable error{(errors.Count == 1 ? string.Empty : "s")}\n");
            foreach (var e in errors)
            {
                s.AppendLine($"  * {e.RecoverableError.ErrorIdentifier.Value} (\"{e.RecoverableError.ErrorMessage.Value}\") at {e.RecoverableError.ErrorTime.Year}-{e.RecoverableError.ErrorTime.Month:00}-{e.RecoverableError.ErrorTime.Day:00}_{e.RecoverableError.ErrorTime.Hour:00}:{e.RecoverableError.ErrorTime.Minute:00}:{e.RecoverableError.ErrorTime.Second:00} in command {e.RecoverableError.CommandIdentifier.Value} (id={e.RecoverableError.CommandExecutionUUID.Value})");
            }

            return s.ToString();
        }

        public static void ConsoleOutput(string output, bool newLine, ConsoleColor color = ConsoleColor.Gray)
        {
            var previousColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.Write(output + (newLine ? "\n" : string.Empty));
            Console.ForegroundColor = previousColor;
        }

        #endregion
    }
}
