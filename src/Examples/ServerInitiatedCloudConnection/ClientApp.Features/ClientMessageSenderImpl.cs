﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Sila2.Org.Silastandard;
using SiLA2.Server.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApp.Features
{
    public class ClientMessageSenderImpl : ClientMessageSender.ClientMessageSenderBase
    {
        private readonly ISiLAClientMessageService _silaClientMessageService;

        public ClientMessageSenderImpl(ISiLAClientMessageService silaClientMessageService)
        {
            _silaClientMessageService = silaClientMessageService;
        }

        public override Task<Empty> SendSiLAClientMessage(SiLAClientMessage request, ServerCallContext context)
        {
            _silaClientMessageService.AddClientMessageRequest(new SiLAClientMessage()
            {
                UnobservablePropertyRead = new UnobservablePropertyRead
                {
                    FullyQualifiedPropertyId = "org.silastandard/core/SiLAService/v1/Property/ServerName"
                }
            }).Wait();
            return Task.FromResult(new Empty());
        }
    }
}
