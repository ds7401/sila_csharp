﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using SiLA2.Client;
using SiLA2.Utils.Extensions;
using SiLA2.Utils.Network;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SiLA2.ReferencingNuget.Client.App
{
    class Program
    {
        private static IConfigurationRoot _configuration;

        static async Task Main(string[] args)
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            _configuration = configBuilder.Build();

            var clientSetup = new Configurator(_configuration, args);

            Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(_configuration)
                    .CreateLogger();

            clientSetup.Container.AddLogging(x => {
                x.ClearProviders();
                x.AddSerilog(dispose: true);
            });
            clientSetup.UpdateServiceProvider();

            var logger = clientSetup.ServiceProvider.GetRequiredService<ILogger<Program>>();

            logger.LogInformation("Starting Server Discovery...");

            var serverMap = await clientSetup.SearchForServers();

            GrpcChannel channel;
            var serverType = "SiLA2TemperatureServer";
            var server = serverMap.Values.FirstOrDefault(x => x.ServerType == serverType);
            if (server != null)
            {
                logger.LogInformation("Found Server");
                logger.LogInformation(server.ServerInfo);
                logger.LogInformation($"Connecting to {server}");
                channel = await clientSetup.GetChannel(server.Address, server.Port, acceptAnyServerCertificate: false, server.SilaCA.GetCaFromFormattedCa());
            }
            else
            {
                var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
                logger.LogInformation($"No connection automatically discovered. Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
                channel = await clientSetup.GetChannel(acceptAnyServerCertificate: true);
            }

            logger.LogInformation("Trying to setup Client ...");
            var clientImpl = new TemperatureControllerClientImpl(channel, clientSetup.ServiceProvider.GetService<ILoggerFactory>());
            logger.LogInformation($"Client Setup successful for following Clients :");
            foreach (var client in clientImpl.Clients)
                logger.LogInformation($"{client.GetType().FullName}");

            // check range validation
            const double targetTemperatureOutOfRangeKelvin = 373.15;
            logger.LogInformation($"Calling \"ControlTemperature({targetTemperatureOutOfRangeKelvin})\" ...");
            await clientImpl.ControlTemperature(targetTemperatureOutOfRangeKelvin);

            await Task.Delay(1000);

            // start temperature control and getting the result without waiting for the execution to be finished
            const double targetTemperatureCelsius = 30;
            logger.LogInformation($"Calling \"ControlTemperature({targetTemperatureCelsius} °C)\" and ControlTemperature_Result() right afterwards");
            clientImpl.ControlTemperatureNoWait(targetTemperatureCelsius.DegreeCelsius2Kelvin());

            await Task.Delay(1000);

            // start temperature control with valid conditions
            logger.LogInformation($"Calling \"ControlTemperature({targetTemperatureCelsius} °C)\" ...");
            await clientImpl.ControlTemperature(targetTemperatureCelsius.DegreeCelsius2Kelvin(), false);

            await Task.Delay(1000);

            // subscribe to temperature property for 5 seconds
            logger.LogInformation("Calling \"Subscribe_CurrentTemperature()\" for 5 seconds ...");
            await clientImpl.GetCurrentTemperature(new TimeSpan(0, 0, 5));

            await Task.Delay(1000);

            // subscribe to temperature property for receiving 3 values
            logger.LogInformation("Calling \"Subscribe_CurrentTemperature()\" for receiving 3 values ...");
            await clientImpl.GetCurrentTemperature(3);

            await Task.Delay(1000);

            // start another temperature control with running property outputs
            logger.LogInformation("Calling \"ControlTemperature(45 °C)\" ...");
            await clientImpl.ControlTemperature(45.0.DegreeCelsius2Kelvin());

            await Task.Delay(1000);

            logger.LogInformation("Shutting down connection...");
            await channel.ShutdownAsync();
            Console.WriteLine();
            Console.WriteLine("Press any key to exit...");

            Console.ReadKey();
        }
    }
}
