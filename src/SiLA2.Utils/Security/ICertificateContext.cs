﻿namespace SiLA2.Utils.Security
{
    public interface ICertificateContext
    {
        string Certificate { get; set; }
        string CERTIFICATE_AUTHORITY_FILE_PATH { get; }
        string CERTIFICATE_AUTHORITY_KEY_FILE_PATH { get; }
        string CERTIFICATE_FILE_PATH { get; }
        string CERTIFICATE_KEY_FILE_PATH { get; }
        string CertificateAuthority { get; set; }
        string CertificateAuthorityKey { get; set; }
        string Key { get; set; }

        void SetCertificateLocation(string folderPath);
    }
}