﻿using System.IO;
using System.Reflection;

namespace SiLA2.Utils.Security
{
    /// <summary>
    /// Denotes a base class for a certificate context
    /// </summary>
    public class CertificateContext : ICertificateContext
    {
        private string _certificateLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        public string CERTIFICATE_FILE_PATH => Path.Combine(_certificateLocation, Constants.SERVER_CRT_FILENAME);
        public string CERTIFICATE_KEY_FILE_PATH => Path.Combine(_certificateLocation, Constants.SERVER_KEY_FILENAME);
        public string CERTIFICATE_AUTHORITY_FILE_PATH => Path.Combine(_certificateLocation, Constants.CA_CRT_FILENAME);
        public string CERTIFICATE_AUTHORITY_KEY_FILE_PATH => Path.Combine(_certificateLocation, Constants.CA_KEY_FILENAME);

        /// <summary>
        /// Gets the PEM-encoded certificate
        /// </summary>
        public string Certificate { get; set; }

        /// <summary>
        /// Gets the PEM-encoded private key for the certificate
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets the PEM-encoded certificate authority used in this context
        /// </summary>
        public string CertificateAuthority { get; set; }

        /// <summary>
        /// Gets the PEM-encoded private key of the certificate authority used in this context
        /// </summary>
        public string CertificateAuthorityKey { get; set; }

        /// <summary>
        /// Change folder path of certificate files. If you do not want to use the standard path (Rootfolder of the Application) you can change the folder path containing the certificate files.
        /// This should be set before the application starts, so the IoC container registration might be a good place to call this method.
        /// </summary>
        /// <param name="folderPath"></param>
        public void SetCertificateLocation(string folderPath)
        {
            _certificateLocation = folderPath;
        }
    }
}
