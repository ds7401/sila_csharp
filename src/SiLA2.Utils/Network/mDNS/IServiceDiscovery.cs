﻿using SiLA2.Utils.Network.Dns;
using SiLA2.Utils.Network.Dns.Resolving;
using System;

namespace SiLA2.Utils.Network.mDNS
{
    public interface IServiceDiscovery
    {
        bool AnswersContainsAdditionalRecords { get; set; }
        MulticastService Mdns { get; }
        NameServer NameServer { get; }

        event EventHandler<DomainName> ServiceDiscovered;
        event EventHandler<ServiceInstanceDiscoveryEventArgs> ServiceInstanceDiscovered;
        event EventHandler<ServiceInstanceShutdownEventArgs> ServiceInstanceShutdown;

        void Advertise(ServiceProfile service);
        void Announce(ServiceProfile profile);
        void Dispose();
        void QueryAllServices();
        void QueryServiceInstances(DomainName service);
        void QueryServiceInstances(DomainName service, string subtype);
        void QueryUnicastAllServices();
        void QueryUnicastServiceInstances(DomainName service);
        void Unadvertise();
        void Unadvertise(ServiceProfile profile);
    }
}