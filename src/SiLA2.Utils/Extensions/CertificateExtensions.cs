﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace SiLA2.Utils.Extensions
{
    public static class CertificateExtensions
    {
        public static X509Certificate2 CompensateWindowsEpheralKeysProblem(this X509Certificate2 cert)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                // SSLStream on Windows throws with ephemeral key sets
                // workaround from https://github.com/dotnet/runtime/issues/23749#issuecomment-388231655
                var originalCert = cert;
                cert = new X509Certificate2(cert.Export(X509ContentType.Pkcs12));
                originalCert.Dispose();
            }
            return cert;
        }

        public static byte[] ExtractFromPem(this string pem)
        {
            var data = PemEncoding.Find(pem);
            return Convert.FromBase64String(pem[data.Base64Data]);
        }

        public static X509Certificate2 GetCaFromFormattedCa(this string ca)
        {
            X509Certificate2 cacert = null;
            var sb = new StringBuilder();
            var caMdnsSplitted = ca.Split("\r\n");
            foreach (var line in caMdnsSplitted)
            {
                if (line.StartsWith("ca"))
                {
                    var firstEquals = line.IndexOf('=');
                    if (firstEquals < 0)
                        continue;
                    sb.Append(line.Substring(firstEquals + 1) + "\n");
                }
            }
            var caSb = sb.ToString();

            var pem = PemEncoding.Find(caSb);
            var certData = Convert.FromBase64String(caSb[pem.Base64Data]);
            cacert = new X509Certificate2(certData);
            return cacert;
        }
    }
}
