﻿using NetMQ;
using System;

namespace SiLA2.IPC.NetMQ
{
    public class ZeroMqServerConfig : IZeroMqServerConfig
    {
        public string ServerSocketAddress { get; }

        /// <summary>
        /// Func Delegate for Server Request Processing Implementation (Default: Returns Request as Response)
        /// </summary>
        public Func<NetMQMessage, string> ProcessRequest { get; set; } = msg => msg.ToString();

        public ZeroMqServerConfig(string serverSocketAddress, Func<NetMQMessage, string> processRequest) : this(serverSocketAddress)
        {
            ProcessRequest = processRequest;
        }

        public ZeroMqServerConfig(string serverSocketAddress)
        {
            ServerSocketAddress = serverSocketAddress;
        }
    }
}
