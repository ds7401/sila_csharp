﻿using NetMQ;
using System;
using System.Threading.Tasks;

namespace SiLA2.IPC.NetMQ
{
    public interface IZeroMqServer
    {
        Task Run();
        void SetRequestProcessing(Func<NetMQMessage, string> requestProcessing);
    }
}