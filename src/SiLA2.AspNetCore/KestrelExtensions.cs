﻿using SiLA2.Utils.CmdArgs.Server;
using SiLA2.Utils.Extensions;
using SiLA2.Utils.Network;
using SiLA2.Utils.Security;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace SiLA2.AspNetCore
{
    public static class KestrelExtensions
    {
        public static Tuple<IPAddress, int, X509Certificate2> GetKestrelConfigData(this string[] args, IServiceProvider serviceProvider)
        {
            var certificateProvider = serviceProvider.GetService(typeof(ICertificateProvider)) as ICertificateProvider;
            var certificate = certificateProvider.GetServerCertificate(safeIfNotExists: true);

            var serverConfig = serviceProvider.GetService(typeof(IServerConfig)) as IServerConfig;

            args.ParseServerCmdLineArgs<CmdLineServerArgs>(serverConfig);

            IPAddress ip = IPAddress.Any;

            if (IPAddress.TryParse(serverConfig.FQHN, out IPAddress ipAddress))
            {
                ip = ipAddress;
            }

            return new Tuple<IPAddress, int, X509Certificate2>(ip, serverConfig.Port, certificate);
        }
    }
}
