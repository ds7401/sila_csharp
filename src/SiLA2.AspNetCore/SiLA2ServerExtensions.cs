﻿using Microsoft.Extensions.Hosting;
using SiLA2.Server;
using System.Reflection;

namespace SiLA2.AspNetCore
{
    public static class WebHostExtensions
    {
        public static void InitializeSiLA2Features(this IHost host, ISiLA2Server siLA2Server)
        {
            var sila2FeatureFiles = Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Features"), "*.sila.xml");
            foreach (var featureFile in sila2FeatureFiles)
            {
                siLA2Server.ReadFeature(featureFile);
            }
        }
    }
}
