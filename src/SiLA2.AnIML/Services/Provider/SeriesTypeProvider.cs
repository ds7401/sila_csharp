﻿using AnIMLCore;

namespace SiLA2.AnIML.Services.Provider
{
    public class SeriesTypeProvider : ISeriesTypeProvider
    {
        public SeriesType GetSeriesType<T>(IEnumerable<T> items, DependencyType dependencyType = DependencyType.independent)
        {
            var seriesId = Guid.NewGuid();
            var seriesType = new SeriesType { name = $"Series {seriesId}", seriesID = $"Series_{seriesId}" };

            if (items.Count() == 0)
                return seriesType;

            var individualValueTypeSet = new IndividualValueSetType();

            switch (typeof(T))
            {
                case Type boolType when boolType == typeof(bool):
                    seriesType.seriesType = ParameterTypeType.Boolean;
                    individualValueTypeSet.ItemsElementName = items.Select(x => ItemsChoiceType3.Boolean).ToArray();
                    break;
                case Type intType when intType == typeof(int):
                    seriesType.seriesType = ParameterTypeType.Int32;
                    individualValueTypeSet.ItemsElementName = items.Select(x => ItemsChoiceType3.I).ToArray();
                    break;
                case Type longType when longType == typeof(long):
                    seriesType.seriesType = ParameterTypeType.Int64;
                    individualValueTypeSet.ItemsElementName = items.Select(x => ItemsChoiceType3.L).ToArray();
                    break;
                case Type floatType when floatType == typeof(float):
                    seriesType.seriesType = ParameterTypeType.Float32;
                    individualValueTypeSet.ItemsElementName = items.Select(x => ItemsChoiceType3.F).ToArray();
                    break;
                case Type doubleType when doubleType == typeof(double):
                    seriesType.seriesType = ParameterTypeType.Float64;
                    individualValueTypeSet.ItemsElementName = items.Select(x => ItemsChoiceType3.D).ToArray();
                    break;
                case Type dateTimeType when dateTimeType == typeof(DateTime):
                    seriesType.seriesType = ParameterTypeType.DateTime;
                    individualValueTypeSet.ItemsElementName = items.Select(x => ItemsChoiceType3.DateTime).ToArray();
                    break;
                default:
                    throw new ArgumentException($"Parameter '{typeof(T)}' is an invalid AnIML type.");
            }

            individualValueTypeSet.Items = items.Cast<object>().ToArray();
            individualValueTypeSet.startIndex = 0;
            individualValueTypeSet.startIndexSpecified = true;
            individualValueTypeSet.endIndex = items.Count() - 1;
            individualValueTypeSet.endIndexSpecified = true;
            seriesType.Items = [individualValueTypeSet];
            seriesType.dependency = dependencyType;
            return seriesType;
        }

        public SeriesType GetSeriesStringType(IEnumerable<string> items, ParameterTypeType stringType, DependencyType dependencyType = DependencyType.independent)
        {
            var seriesId = Guid.NewGuid();
            var seriesType = new SeriesType { name = $"Series {seriesId}", seriesID = $"Series_{seriesId}" };

            if (items.Count() == 0)
                return seriesType;

            var individualValueTypeSet = new IndividualValueSetType();

            switch (stringType)
            {
                case ParameterTypeType.String:
                    seriesType.seriesType = ParameterTypeType.String;
                    individualValueTypeSet.ItemsElementName = items.Select(x => ItemsChoiceType3.S).ToArray();
                    break;
                case ParameterTypeType.SVG:
                    seriesType.seriesType = ParameterTypeType.SVG;
                    individualValueTypeSet.ItemsElementName = items.Select(x => ItemsChoiceType3.SVG).ToArray();
                    break;
                case ParameterTypeType.EmbeddedXML:
                    seriesType.seriesType = ParameterTypeType.EmbeddedXML;
                    individualValueTypeSet.ItemsElementName = items.Select(x => ItemsChoiceType3.EmbeddedXML).ToArray();
                    break;
                default:
                    throw new ArgumentException($"Parameter '{stringType}' is an invalid AnIML string type.");
            }

            individualValueTypeSet.Items = items.Cast<object>().ToArray();
            individualValueTypeSet.startIndex = 0;
            individualValueTypeSet.startIndexSpecified = true;
            individualValueTypeSet.endIndex = items.Count() - 1;
            individualValueTypeSet.endIndexSpecified = true;
            seriesType.Items = [individualValueTypeSet];
            seriesType.dependency = dependencyType;
            return seriesType;
        }

        public SeriesType GetSeriesPNGType(IEnumerable<byte[]> items, DependencyType dependencyType = DependencyType.independent)
        {
            var seriesId = Guid.NewGuid();
            var seriesType = new SeriesType { name = $"Series {seriesId}", seriesID = $"Series_{seriesId}" };

            if (items.Count() == 0)
                return seriesType;

            var individualValueTypeSet = new IndividualValueSetType();
            seriesType.seriesType = ParameterTypeType.PNG;
            individualValueTypeSet.ItemsElementName = items.Select(x => ItemsChoiceType3.PNG).ToArray();
            individualValueTypeSet.Items = items.Select(x => Convert.ToBase64String(x)).Cast<object>().ToArray();
            individualValueTypeSet.startIndex = 0;
            individualValueTypeSet.startIndexSpecified = true;
            individualValueTypeSet.endIndex = items.Count() - 1;
            individualValueTypeSet.endIndexSpecified = true;
            seriesType.Items = [individualValueTypeSet];
            seriesType.dependency = dependencyType;
            return seriesType;
        }
    }
}
