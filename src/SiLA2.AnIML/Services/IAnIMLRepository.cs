﻿using AnIMLCore;
using SiLA2.Database.NoSQL;

namespace SiLA2.AnIML.Services
{
    public interface IAnIMLRepository : IBaseRepository<AnIMLType> { }
}