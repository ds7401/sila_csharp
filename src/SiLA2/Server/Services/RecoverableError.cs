﻿using System;
using System.Collections.Generic;
using Sila2.Org.Silastandard.Core.Errorrecoveryservice.V2;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.Server.Services
{
    public class RecoverableError
    {
        public enum ErrorHandlingState
        {
            Active = 0,
            ErrorHandled,
            Aborted,
            ErrorHandlingTimeoutElapsed
        }

        #region Nested classes

        public class ContinuationOption
        {
            public ContinuationOption()
            {
                Identifier = Description = RequiredInputData = string.Empty;
            }

            public string Identifier { get; set; }

            public string Description { get; set; }

            public string RequiredInputData { get; set; }

            public DataType_ContinuationOption ToSiLAType()
            {
                return new DataType_ContinuationOption
                {
                    ContinuationOption = new DataType_ContinuationOption.Types.ContinuationOption_Struct
                    {
                        Identifier = new SiLAFramework.String { Value = this.Identifier },
                        Description = new SiLAFramework.String { Value = this.Description },
                        RequiredInputData = new SiLAFramework.String { Value = this.RequiredInputData ?? string.Empty }
                    }
                };
            }
        }

        #endregion

        public RecoverableError()
        {
            FullyQualifiedCommandIdentifier = ErrorIdentifier = ErrorMessage = DefaultOption = string.Empty;
            ErrorTime = DateTime.Now;
            ErrorTimeZoneInfo = TimeZoneInfo.Local;
        }

        public string FullyQualifiedCommandIdentifier { get; set; }

        public Guid CommandExecutionUUID { get; set; }

        public string ErrorIdentifier { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime ErrorTime { get; set; }

        public TimeZoneInfo ErrorTimeZoneInfo { get; set; }

        public List<ContinuationOption> ContinuationOptions { get; set; }

        public string DefaultOption { get; set; }

        public TimeSpan AutomaticSelectionTimeout { get; set; }

        /// <summary>Will be set by the ErrorRecoveryService</summary>
        public string SelectedContinuationOption { get; set; }

        /// <summary>Will be set by the ErrorRecoveryService</summary>
        public SiLAFramework.Any InputData { get; set; }

        public DataType_RecoverableError ToSiLAType()
        {
            var result = new DataType_RecoverableError
            {
                RecoverableError = new DataType_RecoverableError.Types.RecoverableError_Struct
                {
                    ErrorIdentifier = new SiLAFramework.String { Value = this.ErrorIdentifier },
                    CommandIdentifier = new SiLAFramework.String { Value = this.FullyQualifiedCommandIdentifier },
                    CommandExecutionUUID = new SiLAFramework.String { Value = this.CommandExecutionUUID.ToString() },
                    ErrorMessage = new SiLAFramework.String { Value = this.ErrorMessage },
                    ErrorTime = new SiLAFramework.Timestamp
                    {
                        Year = (uint)this.ErrorTime.Year,
                        Month = (uint)this.ErrorTime.Month,
                        Day = (uint)this.ErrorTime.Day,
                        Hour = (uint)this.ErrorTime.Hour,
                        Minute = (uint)this.ErrorTime.Minute,
                        Second = (uint)this.ErrorTime.Second,
                        Millisecond = (uint)this.ErrorTime.Millisecond,
                        Timezone = new SiLAFramework.Timezone { Hours = ErrorTimeZoneInfo.BaseUtcOffset.Hours, Minutes = (uint)ErrorTimeZoneInfo.BaseUtcOffset.Minutes }
                    },
                    DefaultOption = new SiLAFramework.String { Value = this.DefaultOption },
                    AutomaticSelectionTimeout = new DataType_Timeout { Timeout = new SiLAFramework.Integer { Value = Convert.ToInt32(AutomaticSelectionTimeout.TotalSeconds) } }
                }
            };

            foreach (var option in this.ContinuationOptions)
            {
                result.RecoverableError.ContinuationOptions.Add(option.ToSiLAType());
            }

            return result;
        }
    }
}
