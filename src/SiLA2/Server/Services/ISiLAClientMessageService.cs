﻿using Sila2.Org.Silastandard;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace SiLA2.Server.Services
{
    public interface ISiLAClientMessageService
    {
        ConcurrentQueue<SiLAClientMessage> ClientMessageRequests { get; }
        Task AddClientMessageRequest(SiLAClientMessage silaClientMessage);
        Task AddServerMessageResponse(SiLAServerMessage silaServerMessage);
        Task<SiLAServerMessage> GetServerResponse(Guid id);
    }
}