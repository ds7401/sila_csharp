﻿using Google.Protobuf;
using SiLA2.Communication.Services;
using SiLA2.Server.Utils;
using UnitTest.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    [TestFixture]
    internal class DynamicClientStructureDataTypeTests
    {
        private GrpcChannel _channel;
        private Feature _silaStructureDataTypeFeature;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            _channel = await ClientBootstrapper.SetupClient();
            _silaStructureDataTypeFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("TestData", "StructureDataTypeTest-v1_0.sila.xml"));
        }


        [Test]
        public void Should_Get_Structure_Property()
        {
            // Arrange
            const string PROPERTY_NAME = "StructureValue";

            const string EXPECTED_STRING_TYPE_VALUE = "SiLA2_Test_String_Value";
            const long EXPECTED_INTEGER_TYPE_VALUE = 5124;
            const double EXPECTED_REAL_TYPE_VALUE = 3.1415926;
            const bool EXPECTED_BOOLEAN_TYPE_VALUE = true;
            SiLAFramework.Binary EXPECTED_BINARY_TYPE_VALUE = new SiLAFramework.Binary { Value = ByteString.CopyFromUtf8("SiLA2_Binary_String_Value") };
            const int EXPECTED_DATE_DAY_VALUE = 5;
            const int EXPECTED_DATE_MONTH_VALUE = 8;
            const int EXPECTED_DATE_YEAR_VALUE = 2022;
            const int EXPECTED_TIME_HOUR_VALUE = 12;
            const int EXPECTED_TIME_MINUTE_VALUE = 34;
            const int EXPECTED_TIME_SECOND_VALUE = 56;
            const int EXPECTED_TIME_MILLISECOND_VALUE = 789;
            SiLAFramework.Any EXPECTED_ANY_TYPE_VALUE = AnyType.CreateAnyTypeObject("SiLA2_Any_Type_String_Value");

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            dynamic response = dynamicMessageService.GetUnobservableProperty(PROPERTY_NAME, _channel, _silaStructureDataTypeFeature);

            // Assert
            Assert.That(response.StructureValue.TestStructure.StringTypeValue.Value, Is.EqualTo(EXPECTED_STRING_TYPE_VALUE));
            Assert.That(response.StructureValue.TestStructure.IntegerTypeValue.Value, Is.EqualTo(EXPECTED_INTEGER_TYPE_VALUE));
            Assert.That(response.StructureValue.TestStructure.RealTypeValue.Value, Is.EqualTo(EXPECTED_REAL_TYPE_VALUE));
            Assert.That(response.StructureValue.TestStructure.BooleanTypeValue.Value, Is.EqualTo(EXPECTED_BOOLEAN_TYPE_VALUE));
            Assert.That(response.StructureValue.TestStructure.BinaryTypeValue.Value, Is.EqualTo(EXPECTED_BINARY_TYPE_VALUE.Value.Span.ToArray()));
            Assert.That(response.StructureValue.TestStructure.DateTypeValue.Year, Is.EqualTo(EXPECTED_DATE_YEAR_VALUE));
            Assert.That(response.StructureValue.TestStructure.DateTypeValue.Month, Is.EqualTo(EXPECTED_DATE_MONTH_VALUE));
            Assert.That(response.StructureValue.TestStructure.DateTypeValue.Day, Is.EqualTo(EXPECTED_DATE_DAY_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimeTypeValue.Hour, Is.EqualTo(EXPECTED_TIME_HOUR_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimeTypeValue.Minute, Is.EqualTo(EXPECTED_TIME_MINUTE_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimeTypeValue.Second, Is.EqualTo(EXPECTED_TIME_SECOND_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimeTypeValue.Millisecond, Is.EqualTo(EXPECTED_TIME_MILLISECOND_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Year, Is.EqualTo(EXPECTED_DATE_YEAR_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Month, Is.EqualTo(EXPECTED_DATE_MONTH_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Day, Is.EqualTo(EXPECTED_DATE_DAY_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Hour, Is.EqualTo(EXPECTED_TIME_HOUR_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Minute, Is.EqualTo(EXPECTED_TIME_MINUTE_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Second, Is.EqualTo(EXPECTED_TIME_SECOND_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Millisecond, Is.EqualTo(EXPECTED_TIME_MILLISECOND_VALUE));
            Assert.That(response.StructureValue.TestStructure.AnyTypeValue.Payload, Is.EqualTo(EXPECTED_ANY_TYPE_VALUE.Payload.Span.ToArray()));
        }
    }
}
