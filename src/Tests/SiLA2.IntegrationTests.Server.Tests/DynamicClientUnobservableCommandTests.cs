﻿using SiLA2.Communication.Services;
using SiLA2.Server.Utils;
using UnitTest.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    //TODO: UnaryAsync to be immplemented yet

    [TestFixture]
    internal class DynamicClientUnobservableCommandTests
    {
        private GrpcChannel _channel;
        private Feature _silaUnobservableCommandFeature;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            _channel = await ClientBootstrapper.SetupClient();
            _silaUnobservableCommandFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("TestData","UnobservableCommandTest-v1_0.sila.xml"));
        }

        [Test]
        public void Should_Call_UnobservableCommand_Without_Parameters_And_Responses()
        {
            // Arrange
            var operationName = "CommandWithoutParametersAndResponses";

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            var response = dynamicMessageService.ExecuteUnobservableCommand(operationName, _channel, _silaUnobservableCommandFeature);

            // Assert
            Assert.That(response != null);
        }

        [Test]
        public void Should_Call_UnobservableCommand_With_Parameter()
        {
            // Arrange
            const string EXPECTED_RESULT = "12345";

            var operationName = "ConvertIntegerToString";
            var responseProperty = "StringRepresentation";
            IDictionary<string, object> payloadMap = new Dictionary<string, object> { { "Integer", new SiLAFramework.Protobuf.Integer { Value = 12345 } } };

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());
            
            // Act
            var response = dynamicMessageService.ExecuteUnobservableCommand(operationName, _channel, _silaUnobservableCommandFeature, payloadMap);

            // Assert
            var propertyInfo = response.GetType().GetProperty(responseProperty).GetValue(response, null);
            var propertyValue = propertyInfo.GetType().GetProperty("Value").GetValue(propertyInfo, null);
            Assert.That(propertyValue.ToString() == EXPECTED_RESULT);
        }
        
        [Test]
        public void Should_Call_UnobservableCommand_With_Parameters()
        {
            // Arrange
            const string EXPECTED_RESULT = "123abc";

            var operationName = "JoinIntegerAndString";
            var responseProperty = "JoinedParameters";
            IDictionary<string, object> payloadMap = new Dictionary<string, object> { { "Integer", new SiLAFramework.Protobuf.Integer { Value = 123 } }, { "String", new SiLAFramework.Protobuf.String { Value = "abc" } } };

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            var response = dynamicMessageService.ExecuteUnobservableCommand(operationName, _channel, _silaUnobservableCommandFeature, payloadMap);

            // Assert
            var propertyInfo = response.GetType().GetProperty(responseProperty).GetValue(response, null);
            var propertyValue = propertyInfo.GetType().GetProperty("Value").GetValue(propertyInfo, null);
            Assert.That(propertyValue.ToString() == EXPECTED_RESULT);
        }

        [Test]
        public void Should_Call_Unobservable_Command_And_Return_Multiple_Responses()
        {
            // Arrange
            const string EXPECTED_RESULT1 = "a";
            const string EXPECTED_RESULT2 = "bcde";

            var operationName = "SplitStringAfterFirstCharacter";
            var responseProperty1 = "FirstCharacter";
            var responseProperty2 = "Remainder";
            IDictionary<string, object> payloadMap = new Dictionary<string, object> { { "String", new SiLAFramework.Protobuf.String { Value = "abcde" } } };

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            var response = dynamicMessageService.ExecuteUnobservableCommand(operationName, _channel, _silaUnobservableCommandFeature, payloadMap);

            // Assert
            var propertyInfo1 = response.GetType().GetProperty(responseProperty1).GetValue(response, null);
            var propertyValue1 = propertyInfo1.GetType().GetProperty("Value").GetValue(propertyInfo1, null);
            Assert.That(propertyValue1.ToString() == EXPECTED_RESULT1);
            var propertyInfo2 = response.GetType().GetProperty(responseProperty2).GetValue(response, null);
            var propertyValue2 = propertyInfo2.GetType().GetProperty("Value").GetValue(propertyInfo2, null);
            Assert.That(propertyValue2.ToString() == EXPECTED_RESULT2);
        }
    }
}
