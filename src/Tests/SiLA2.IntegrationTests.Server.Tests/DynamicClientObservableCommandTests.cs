﻿using NUnit.Framework.Internal;
using Sila2.Org.Silastandard;
using SiLA2.Communication.Services;
using SiLA2.Server.Utils;
using UnitTest.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    internal class DynamicClientObservableCommandTests
    {
        private GrpcChannel _channel;
        private Feature _silaObservableCommandFeature;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            _channel = await ClientBootstrapper.SetupClient();
            _silaObservableCommandFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("TestData", "ObservableCommandTest-v1_0.sila.xml"));
        }

        //TODO: Add IntermediateResponeses Test Part
        [Test]
        public async Task Should_Call_ObservableCommand_With_Parameters()
        {
            // Arrange
            const long EXPECTED_ITERATIONS = 4;
            const string OPERATION_NAME = "Count";
            const string RESPONSE_PROPERTY = "IterationResponse";
            IDictionary<string, object> payloadMap = new Dictionary<string, object> { { "N", new SiLAFramework.Protobuf.Integer { Value = 5 } }, { "Delay", new SiLAFramework.Protobuf.Real { Value = 1.5 } } };

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            var response = dynamicMessageService.ExecuteObservableCommand(OPERATION_NAME, _channel, _silaObservableCommandFeature, payloadMap);

            var asyncEnumerator = response.Item2.GetAsyncEnumerator();

            List<ExecutionInfo> list = new();
            while (await asyncEnumerator.MoveNextAsync())
            {
                list.Add(asyncEnumerator.Current);
                if(asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully 
                    || asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError)
                {
                    break;
                }
            }

            var result = dynamicMessageService.GetObservableCommandResult(response.Item1.CommandExecutionUUID, OPERATION_NAME, _channel, _silaObservableCommandFeature, response.Item3);
            long iterations = ((SiLAFramework.Protobuf.Integer)(result.GetType().GetProperty(RESPONSE_PROPERTY).GetValue(result, null))).Value;

            // Assert
            Assert.That(list.Count, Is.GreaterThan(5));
            Assert.That(iterations, Is.EqualTo(EXPECTED_ITERATIONS));
        }
    }
}
