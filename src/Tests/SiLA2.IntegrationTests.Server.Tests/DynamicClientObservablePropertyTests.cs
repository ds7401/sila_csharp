﻿using ProtoBuf;
using ProtoBuf.Grpc.Client;
using SiLA2.Communication.Protobuf;
using SiLA2.Communication.Services;
using SiLA2.Server.Utils;
using UnitTest.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    [TestFixture]
    internal class DynamicClientObservablePropertyTests
    {
        private GrpcChannel _channel;
        private Feature _silaObservablePropertyFeature;

        [ProtoContract]
        public class ObservablePropertyResponse
        {
            [ProtoMember(1)]
            public SiLAFramework.Protobuf.Integer FixedValue { get; set; }
        }

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            _channel = await ClientBootstrapper.SetupClient();
            _silaObservablePropertyFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("TestData", "ObservablePropertyTest-v1_0.sila.xml"));
        }

        [Test]
        public async Task Should_Get_Fixed_Observable_Property_By_ProtobufGrpcClient()
        {
            // Arrange
            const long EXPECTED_RESULT = 42;
            var grpcService = $"{_silaObservablePropertyFeature.Namespace}.{_silaObservablePropertyFeature.Identifier}";
            var propertyName = "FixedValue";
            var operationName = $"Subscribe_{propertyName}";

            // System under Test
            var client = new GrpcClient(_channel.CreateCallInvoker(), grpcService);

            // Act
            IAsyncEnumerable<ObservablePropertyResponse> response = client.ServerStreamingAsync<EmptyMessage, ObservablePropertyResponse>(new EmptyMessage(), operationName);

            long result = 0;

            IAsyncEnumerator<ObservablePropertyResponse> asyncEnumerator = response.GetAsyncEnumerator();
           
            while (await asyncEnumerator.MoveNextAsync())
            {
                result = asyncEnumerator.Current.FixedValue.Value;
            }

            // Assert
            Assert.That(result, Is.EqualTo(EXPECTED_RESULT));
        }


        [Test]
        public async Task Should_Get_Fixed_Observable_Property()
        {
            // Arrange
            var propertyName = "FixedValue";
            const long EXPECTED_RESULT = 42;

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            var response = dynamicMessageService.SubcribeObservableProperty(propertyName, _channel, _silaObservablePropertyFeature);

            long result = 0;

            var asyncEnumerator = response.GetAsyncEnumerator();

            while (await asyncEnumerator.MoveNextAsync())
            {
                var responseResultPropertyValue = asyncEnumerator.Current.GetType().GetProperty(propertyName).GetValue(asyncEnumerator.Current, null);
                result = (long)responseResultPropertyValue.GetType().GetProperty("Value").GetValue(responseResultPropertyValue, null);
            }

            // Assert
            Assert.That(result, Is.EqualTo(EXPECTED_RESULT));
        }
    }
}
