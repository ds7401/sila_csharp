﻿using SiLA2.Communication.Services;
using SiLA2.Server.Utils;
using UnitTest.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    [TestFixture]
    internal class DynamicClientListDataTypeTests
    {
        private GrpcChannel _channel;
        private Feature _silaListDataTypeFeature;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            _channel = await ClientBootstrapper.SetupClient();
            _silaListDataTypeFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("TestData", "ListDataTypeTest-v1_0.sila.xml"));
        }

        [Test]
        public void Should_Receive_Empty_List()
        {
            //Arrange
            const string PROPERTY_NAME = "EmptyStringList";

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            var response = dynamicMessageService.GetUnobservableProperty(PROPERTY_NAME, _channel, _silaListDataTypeFeature);

            // Assert
            var propertyInfo = response.GetType().GetProperty(PROPERTY_NAME).GetValue(response, null);
            Assert.That(propertyInfo == null);
        }


        [Test]
        public void Should_Return_Same_ListDataTypeValues_As_Passed_In_As_Parameter()
        {
            //Arrange
            const string OPERATION_NAME = "EchoStringList";

            const string LIST_DATA_VALUE_1 = "SiLA2";
            const string LIST_DATA_VALUE_2 = "ListDataType";
            const string LIST_DATA_VALUE_3 = "Test";

            var list = new Google.Protobuf.Collections.RepeatedField<SiLAFramework.Protobuf.String>
            {
                new SiLAFramework.Protobuf.String { Value = LIST_DATA_VALUE_1 },
                new SiLAFramework.Protobuf.String { Value = LIST_DATA_VALUE_2 },
                new SiLAFramework.Protobuf.String { Value = LIST_DATA_VALUE_3 }
            };

            IDictionary<string, object> payloadMap = new Dictionary<string, object> { { "StringList", list } };

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            dynamic response = dynamicMessageService.ExecuteUnobservableCommand(OPERATION_NAME, _channel, _silaListDataTypeFeature, payloadMap);

            // Assert
            Assert.That(response.ReceivedValues[0].Value, Is.EqualTo(LIST_DATA_VALUE_1));
            Assert.That(response.ReceivedValues[1].Value, Is.EqualTo(LIST_DATA_VALUE_2));
            Assert.That(response.ReceivedValues[2].Value, Is.EqualTo(LIST_DATA_VALUE_3));
        }
    }
}
