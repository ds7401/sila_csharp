﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using SiLA2.Client;
using SiLA2.Client.Dynamic;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;

namespace UnitTest.Utils
{
    public class ClientBootstrapper
    {
        public static async Task<GrpcChannel> SetupClient()
        {
            var configBuilder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            var clientSetup = new Configurator(configuration, []);

            IClientConfig clientConfig = (IClientConfig)clientSetup.ServiceProvider.GetService(typeof(IClientConfig));
            Console.WriteLine($"Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
            return await ((IGrpcChannelProvider)clientSetup.ServiceProvider.GetService(typeof(IGrpcChannelProvider))).GetChannel(clientConfig.IpOrCdirOrFullyQualifiedHostName, clientConfig.Port, accceptAnyServerCertificate: true);
        }

        public static async Task<IList<IDynamicConfigurator>> SetupClients(int count)
        {
            List<IDynamicConfigurator> clients = new List<IDynamicConfigurator>();
            for (int i = 0; i < count; i++)
            {
                var configBuilder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                var configuration = configBuilder.Build();

                var clientSetup = new DynamicConfigurator(configuration, []);
                clients.Add(clientSetup);
            }
            return clients;
        }
    }
}
