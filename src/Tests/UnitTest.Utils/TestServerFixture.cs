﻿using Grpc.Net.Client;
using Microsoft.AspNetCore.Mvc.Testing;

namespace UnitTest.Utils
{
    public sealed class TestServerFixture<T> : IDisposable where T : class
    {
        private readonly WebApplicationFactory<T> _factory;

        public IServiceProvider ServiceProvider => _factory.Services;

        public TestServerFixture(string[] args, int? port = null)
        {
            Args = args;
            _factory = new WebApplicationFactory<T>();
            var client = _factory.CreateDefaultClient(new ResponseVersionHandler());
            var address = port.HasValue ? new Uri($"https://localhost:{port.Value}") : client.BaseAddress;
            GrpcChannel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
            {
                HttpClient = client
            });
        }

        public GrpcChannel GrpcChannel { get; }

        public string[] Args { get; set; }

        public void Dispose()
        {
            _factory.Dispose();
        }

        private class ResponseVersionHandler : DelegatingHandler
        {
            protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                var response = await base.SendAsync(request, cancellationToken);
                response.Version = request.Version;
                return response;
            }
        }
    }
}
