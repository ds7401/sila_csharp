﻿using System.IO;
using System.Xml.Serialization;

namespace SiLA2.Server.Tests
{
    internal class TestHelper
    {
        public static Feature GetFeatureFromFile(string path)
        {
            Feature feature;
            using (var fs = File.OpenRead(path))
            {
                var serializer = new XmlSerializer(typeof(Feature));
                feature = (Feature)serializer.Deserialize(fs);
            }

            return feature;
        }
    }
}
