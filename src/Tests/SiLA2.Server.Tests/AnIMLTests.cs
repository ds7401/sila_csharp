﻿using AnIMLCore;
using NUnit.Framework;
using SiLA2.AnIML;
using SiLA2.AnIML.Services.Builder;
using SiLA2.AnIML.Services.Provider;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using AnIMLTechniqueNsAlias = AnIMLTechnique;

namespace SiLA2.Server.Tests
{
    [TestFixture]
    internal class AnIMLTests
    {
        const string TEST_FOLDER = "TestData";

        [TestCase("microplate-sila.animl", 5, 1, 4.749, 5.652)]
        public void Should_Deserialize_AnIML_From_File(string fileName, int experimentStepCount, int focusedSeries, double firstSeriesValue, double lastSeriesValue)
        {
            // Arrange
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;

            var filePath = $"{Path.Combine(TEST_FOLDER, fileName)}";
            AnIMLType aniML = null;

            // System under Test
            XmlSerializer serializer = new XmlSerializer(typeof(AnIMLType));

            // Act
            using (var fs = File.OpenRead(filePath))
            {
                using (XmlReader reader = XmlReader.Create(fs, settings))
                {
                    aniML = (AnIMLType)serializer.Deserialize(reader);
                }
            }

            // Assert
            Assert.That(aniML != null);
            Assert.That(aniML.ExperimentStepSet.ExperimentStep.Length, Is.EqualTo(experimentStepCount));
            var valueSet = (IndividualValueSetType)aniML.ExperimentStepSet.ExperimentStep[0].Result[0].SeriesSet.Series[focusedSeries].Items[0];
            Assert.That(valueSet.Items[0], Is.EqualTo(firstSeriesValue));
            Assert.That(valueSet.Items[valueSet.Items.Length - 1], Is.EqualTo(lastSeriesValue));
        }

        [TestCase("microplate-read.atdd")]
        [TestCase("chromatography.atdd")]
        public void Should_Deserialize_AnIML_Technique_From_File(string fileName)
        {
            // Arrange
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.IgnoreWhitespace = true;

            var filePath = $"{Path.Combine(TEST_FOLDER, fileName)}";
            AnIMLTechniqueNsAlias.TechniqueType animlTechnique = null;

            // System under Test
            XmlSerializer serializer = new XmlSerializer(typeof(AnIMLTechniqueNsAlias.TechniqueType));

            // Act
            using (var fs = File.OpenRead(filePath))
            {
                using (XmlReader reader = XmlReader.Create(fs, settings))
                {
                    animlTechnique = (AnIMLTechniqueNsAlias.TechniqueType)serializer.Deserialize(reader);
                }
            }

            // Assert
            Assert.That(animlTechnique != null);
        }

        [Test]
        public void Should_Validate_AnIML_Xml()
        {
            // Arrange
            string ANIML_TEST_DOCUMENT = Path.Combine(TEST_FOLDER, "microplate-sila.animl");

            // System under Test
            XDocument xDoc = null;

            using (var stream = File.OpenRead(ANIML_TEST_DOCUMENT))
            {
                xDoc = XDocument.Load(stream);
            }

            // Act & Assert
            Assert.DoesNotThrow(xDoc.ValidateAnIMLCoreDocument);
        }

        [TestCase("microplate-sila.animl")]
        public void Should_Serialize_AnIML_To_Xml(string fileName)
        {
            // Arrange
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;

            var filePath = $"{Path.Combine(TEST_FOLDER, fileName)}";
            AnIMLType aniML = null;

            XmlSerializer serializer = new XmlSerializer(typeof(AnIMLType));

            using (var fs = File.OpenRead(filePath))
            {
                using (XmlReader reader = XmlReader.Create(fs, settings))
                {
                    aniML = (AnIMLType)serializer.Deserialize(reader);
                }
            }

            // System under Test & Act
            var result = aniML.GetAnIMLXml();

            // Assert
            Assert.That(string.IsNullOrEmpty(result), Is.EqualTo(false));
        }


        [Test]
        public void Should_Serialize_AnIML_With_Custom_Series_To_Xml()
        {
            // Arrange
            var dateTime0 = DateTime.Now;
            var dateTime1 = dateTime0.AddSeconds(1);
            var dateTime2 = dateTime0.AddSeconds(2);
            object[] ts = new object[] { dateTime0, dateTime1, dateTime2 };
            double doubleVal0 = 1.1d;
            double doubleVal1 = 2.2d;
            double doubleVal2 = 3.141592d;
            object[] vals = new object[] { doubleVal0, doubleVal1, doubleVal2 };
            var cultureInfoEN = new CultureInfo("en-En");

            // System under Test
            var seriesBuilder = new SeriesTypeBuilder();
            var animl = seriesBuilder.Build(new string[] { $"Unit Test Sample {DateTime.Now}" });
            var seriesSet = animl.CreateAnIMLSeries(2);

            seriesSet.Series[0].seriesType = ParameterTypeType.DateTime;
            var valueSetType0 = new IndividualValueSetType
            {
                Items = ts,
                ItemsElementName = ts.Select(x => ItemsChoiceType3.DateTime).ToArray(),
                startIndex = 0,
                endIndex = ts.Count() - 1
            };
            seriesSet.Series[0].Items = new object[] { valueSetType0 };

            seriesSet.Series[1].seriesType = ParameterTypeType.Float64;
            seriesSet.Series[1].dependency = DependencyType.dependent;
            var valueSetType1 = new IndividualValueSetType
            {
                Items = vals,
                ItemsElementName = vals.Select(x => ItemsChoiceType3.D).ToArray(),
                startIndex = 0,
                endIndex = vals.Count() - 1 
            };
            seriesSet.Series[1].Items = new object[] { valueSetType1 };

            // Act
            var result = animl.GetAnIMLXml();

            // Assert
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(result);

            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series/IndividualValueSet/DateTime
            var dateTimeNode0 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0];
            Assert.That(DateTime.Parse(dateTimeNode0.InnerText), Is.EqualTo(dateTime0));
            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series/IndividualValueSet/DateTime[1]
            var dateTimeNode1 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1];
            Assert.That(DateTime.Parse(dateTimeNode1.InnerText), Is.EqualTo(dateTime1));
            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series/IndividualValueSet/DateTime[2]
            var dateTimeNode2 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[2];
            Assert.That(DateTime.Parse(dateTimeNode2.InnerText), Is.EqualTo(dateTime2));
            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series[2]/IndividualValueSet/D
            var doubleValue0 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0];
            Assert.That(double.Parse(doubleValue0.InnerText, NumberStyles.AllowDecimalPoint, cultureInfoEN), Is.EqualTo(doubleVal0));
            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series[2]/IndividualValueSet/D[1]
            var doubleValue1 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1];
            Assert.That(double.Parse(doubleValue1.InnerText, NumberStyles.AllowDecimalPoint, cultureInfoEN), Is.EqualTo(doubleVal1));
            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series[2]/IndividualValueSet/D[2]
            var doubleValue2 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2];
            Assert.That(double.Parse(doubleValue2.InnerText, NumberStyles.AllowDecimalPoint, cultureInfoEN), Is.EqualTo(doubleVal2));
        }


        [TestCase(new bool[] { true, false, true }, ParameterTypeType.Boolean)]
        [TestCase(new int[] { int.MinValue, 23, int.MaxValue }, ParameterTypeType.Int32)]
        [TestCase(new long[] { long.MinValue, 23L, long.MaxValue }, ParameterTypeType.Int64)]
        [TestCase(new float[] { float.MinValue, 2.33f, float.MaxValue }, ParameterTypeType.Float32)]
        [TestCase(new double[] { double.MinValue, 2.3d, double.MaxValue }, ParameterTypeType.Float64)]
        public void Should_Create_Series_By_Parameters<T>(IEnumerable<T> data, ParameterTypeType seriesType)
        {
            // Arrange

            // System under Test
            var seriesTypeProvider = new SeriesTypeProvider();

            // Act
            var series = seriesTypeProvider.GetSeriesType(data, DependencyType.dependent);

            // Assert
            Assert.That(((IndividualValueSetType)series.Items[0]).Items.Length, Is.EqualTo(data.Count()));
            Assert.That(series.seriesType, Is.EqualTo(seriesType));
        }

        [Test]
        public void Should_Create_String_Series_By_String_Parameters()
        {
            // Arrange
            const ParameterTypeType STRING_PARAMETER_TYPE = ParameterTypeType.String;
            IEnumerable<string> data = new string[] { "AnIML", "String Test", "Sequence" };

            // System under Test
            var seriesTypeProvider = new SeriesTypeProvider();

            // Act
            var series = seriesTypeProvider.GetSeriesStringType(data, STRING_PARAMETER_TYPE, DependencyType.dependent);

            // Assert
            Assert.That(((IndividualValueSetType)series.Items[0]).Items.Length, Is.EqualTo(data.Count()));
            Assert.That(series.seriesType, Is.EqualTo(STRING_PARAMETER_TYPE));
        }

        [Test]
        public void Should_Create_SVG_Series_By_String_Parameters()
        {
            // Arrange
            const ParameterTypeType STRING_PARAMETER_TYPE = ParameterTypeType.SVG;
            const string SVG0 = "<svg version=\"1.1\" width=\"300\" height=\"200\" xmlns=\"http://www.w3.org/2000/svg\">\r\n  <rect width=\"100%\" height=\"100%\" fill=\"red\" />\r\n  <circle cx=\"150\" cy=\"100\" r=\"80\" fill=\"green\" />\r\n  <text x=\"150\" y=\"125\" font-size=\"60\" text-anchor=\"middle\" fill=\"white\">SVG</text>\r\n</svg>";
            const string SVG1 = "<svg height=\"210\" width=\"400\">\r\n  <path d=\"M150 0 L75 200 L225 200 Z\" />\r\n  Sorry, your browser does not support inline SVG.\r\n</svg>";
            IEnumerable<string> data = new string[] { SVG0, SVG1 };

            // System under Test
            var seriesTypeProvider = new SeriesTypeProvider();

            // Act
            var series = seriesTypeProvider.GetSeriesStringType(data, STRING_PARAMETER_TYPE, DependencyType.dependent);

            // Assert
            Assert.That(((IndividualValueSetType)series.Items[0]).Items.Length, Is.EqualTo(data.Count()));
            Assert.That(series.seriesType, Is.EqualTo(STRING_PARAMETER_TYPE));
        }

        [Test]
        public void Should_Create_EmbeddedXml_Series_By_String_Parameters()
        {
            // Arrange
            const ParameterTypeType STRING_PARAMETER_TYPE = ParameterTypeType.EmbeddedXML;
            const string EMBEDDED_XML_0 = "<div class=\"myDiv\">\r\n  <h2>This is a heading in a div element 1</h2>\r\n  <p>This is some text in a div element.</p>\r\n</div>\r\n";
            const string EMBEDDED_XML_1 = "<div class=\"myDiv\">\r\n  <h2>This is a heading in a div element 2</h2>\r\n  <p>This is some text in a div element.</p>\r\n</div>\r\n";
            const string EMBEDDED_XML_2 = "<div class=\"myDiv\">\r\n  <h2>This is a heading in a div element 3</h2>\r\n  <p>This is some text in a div element.</p>\r\n</div>\r\n";
            IEnumerable<string> data = new string[] { EMBEDDED_XML_0, EMBEDDED_XML_1, EMBEDDED_XML_2 };

            // System under Test
            var seriesTypeProvider = new SeriesTypeProvider();

            // Act
            var series = seriesTypeProvider.GetSeriesStringType(data, STRING_PARAMETER_TYPE, DependencyType.dependent);

            // Assert
            Assert.That(((IndividualValueSetType)series.Items[0]).Items.Length, Is.EqualTo(data.Count()));
            Assert.That(series.seriesType, Is.EqualTo(STRING_PARAMETER_TYPE));
        }


        [Test]
        public void Should_Create_DateTime_Series()
        {
            // Arrange
            const ParameterTypeType DATETIME_PARAMETER_TYPE = ParameterTypeType.DateTime;
            var dateTime0 = DateTime.Now;
            var dateTime1 = dateTime0.AddSeconds(1);
            var dateTime2 = dateTime0.AddSeconds(2);

            IEnumerable<DateTime> data = new DateTime[] { dateTime0, dateTime1, dateTime2 };

            // System under Test
            var seriesTypeProvider = new SeriesTypeProvider();

            // Act
            var series = seriesTypeProvider.GetSeriesType(data, DependencyType.dependent);

            // Assert
            Assert.That(((IndividualValueSetType)series.Items[0]).Items.Length, Is.EqualTo(data.Count()));
            Assert.That(series.seriesType, Is.EqualTo(DATETIME_PARAMETER_TYPE));
        }

        [Test]
        public void Should_Create_PNG_Series()
        {
            // Arrange
            const ParameterTypeType PNG_PARAMETER_TYPE = ParameterTypeType.PNG;
            var png0 = new byte[] { 0x1, 0x2, 0xFF };
            var png1 = new byte[] { 0xEE, 0xDD, 0xAA };
            IEnumerable<byte[]> data = [png0, png1];

            // System under Test
            var seriesTypeProvider = new SeriesTypeProvider();

            // Act
            var series = seriesTypeProvider.GetSeriesPNGType(data, DependencyType.dependent);

            // Assert
            Assert.That(((IndividualValueSetType)series.Items[0]).Items.Length, Is.EqualTo(data.Count()));
            var png0Series = Convert.FromBase64String(((IndividualValueSetType)series.Items[0]).Items[0].ToString());
            Assert.That(png0, Is.EqualTo(png0Series));
            var png1Series = Convert.FromBase64String(((IndividualValueSetType)series.Items[0]).Items[1].ToString());
            Assert.That(png1, Is.EqualTo(png1Series));
            Assert.That(series.seriesType, Is.EqualTo(PNG_PARAMETER_TYPE));
        }
    }   
}
