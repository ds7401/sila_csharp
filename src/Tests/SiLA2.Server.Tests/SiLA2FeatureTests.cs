﻿using NUnit.Framework;
using SiLA2.Server.Utils;

namespace SiLA2.Server.Tests
{
    [TestFixture]
    internal class SiLA2FeatureTests
    {

        [Test]
        public void Should_Read_Feature_From_Online_Ressource()
        {
            // Arrange
            const string URL = "https://gitlab.com/SiLA2/sila_base/-/raw/master/feature_definitions/org/silastandard/core/SiLAService-v1_0.sila.xml";
            const string EXPECTED_FULLY_QUALIFIED_FEATURE_IDENTIFIER = "org.silastandard/core/SiLAService/v1";


            // System under Test & Act
            var onlineFeature = FeatureGenerator.ReadFeatureFromOnlineResource(URL);

            // Assert
            Assert.That(onlineFeature.FullyQualifiedIdentifier, Is.EqualTo(EXPECTED_FULLY_QUALIFIED_FEATURE_IDENTIFIER));
        }
    }
}
