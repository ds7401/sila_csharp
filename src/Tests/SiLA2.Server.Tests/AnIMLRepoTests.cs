﻿using NUnit.Framework;
using SiLA2.AnIML.Services;
using SiLA2.AnIML;
using SiLA2.AnIML.Services.Builder;
using SiLA2.AnIML.Services.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Globalization;

namespace SiLA2.Server.Tests
{
    [TestFixture]
    internal class AnIMLRepoTests
    {
        [Test]
        public void Should_Convert_DbLite_Bson_To_XSD_Validated_AnIML()
        {
            // Arrange
            var cultureInfoEN = new CultureInfo("en-En");
            var db = new LiteDB.LiteDatabase("Filename=test.db;mode=shared");
            var repo = new AnIMLRepository(db);

            var ts = DateTime.Now;
            var seriesTypeBuilder = new SeriesTypeBuilder();
            var animl = seriesTypeBuilder.Build(new string[] { $"Temperature Sample {ts}" });
            var seriesSet = animl.CreateAnIMLSeries(2);
            var values = new List<Tuple<double, DateTime>> { new Tuple<double, DateTime>(1.1, ts), new Tuple<double, DateTime>(2.2, ts.AddHours(1)), new Tuple<double, DateTime>(3.3, ts.AddHours(2)) };
            var seriesProvider = new SeriesTypeProvider();
            seriesSet.Series[0] = seriesProvider.GetSeriesType(values.Select(x => x.Item2), AnIMLCore.DependencyType.dependent);
            seriesSet.Series[1] = seriesProvider.GetSeriesType(values.Select(x => x.Item1), AnIMLCore.DependencyType.dependent);
            animl.ExperimentStepSet.id = $"ExperimentStepSetId_{ts.ToString().Replace('/','.').Replace(' ', '_').Replace(':','.')}";
            repo.Create(animl, out object id);
            var aniMLFromDb = repo.FindById(id);

            // System under Test & Act
            var result = aniMLFromDb.GetAnIMLXml();

            // Assert
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(result);

            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series/IndividualValueSet/DateTime
            var dateTimeNode0 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0];
            Assert.That(DateTime.Parse(dateTimeNode0.InnerText).ToUniversalTime().ToString("G"), Is.EqualTo(values[0].Item2.ToUniversalTime().ToString("G")));
            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series/IndividualValueSet/DateTime[1]
            var dateTimeNode1 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1];
            Assert.That(DateTime.Parse(dateTimeNode1.InnerText).ToUniversalTime().ToString("G"), Is.EqualTo(values[1].Item2.ToUniversalTime().ToString("G")));
            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series/IndividualValueSet/DateTime[2]
            var dateTimeNode2 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[2];
            Assert.That(DateTime.Parse(dateTimeNode2.InnerText).ToUniversalTime().ToString("G"), Is.EqualTo(values[2].Item2.ToUniversalTime().ToString("G")));
            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series[2]/IndividualValueSet/D
            var doubleValue0 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0];
            Assert.That(double.Parse(doubleValue0.InnerText, NumberStyles.AllowDecimalPoint, cultureInfoEN), Is.EqualTo(values[0].Item1));
            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series[2]/IndividualValueSet/D[1]
            var doubleValue1 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1];
            Assert.That(double.Parse(doubleValue1.InnerText, NumberStyles.AllowDecimalPoint, cultureInfoEN), Is.EqualTo(values[1].Item1));
            // XPath /AnIML/ExperimentStepSet/ExperimentStep/Result/SeriesSet/Series[2]/IndividualValueSet/D[2]
            var doubleValue2 = xmlDoc.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2];
            Assert.That(double.Parse(doubleValue2.InnerText, NumberStyles.AllowDecimalPoint, cultureInfoEN), Is.EqualTo(values[2].Item1));
        }
    }
}
