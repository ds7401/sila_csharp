﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using SiLAFramework = Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Test.Metadataconsumertest.V1;
using Sila2.Org.Silastandard.Test.Metadataprovider.V1;
using SiLA2.Server;
using SiLA2.Server.Utils;
using SiLA2.Utils;

namespace SiLA2.IntegrationTests.Features.Service.Implementations
{
    public class MetadataConsumerTestServiceImpl : MetadataConsumerTest.MetadataConsumerTestBase
    {
        private readonly ILogger<MetadataConsumerTestServiceImpl> _logger;
        private readonly ISiLA2Server _silaServer;
        private readonly Feature _metadataProvider;

        public MetadataConsumerTestServiceImpl(ISiLA2Server silaServer, ILogger<MetadataConsumerTestServiceImpl> logger)
        {
            _logger = logger;
            _silaServer = silaServer;
            _silaServer.ReadFeature(Path.Combine("Features", "MetadataConsumerTest-v1_0.sila.xml"));
            _metadataProvider = _silaServer.ReadFeature(Path.Combine("Features", "MetadataProvider-v1_0.sila.xml"));
        }

        public override Task<EchoStringMetadata_Responses> EchoStringMetadata(EchoStringMetadata_Parameters request, ServerCallContext context)
        {
            // get value of metadata entry
            var metadataValue = string.Empty;
            try
            {
                // extract metadata object
                byte[] value = SilaClientMetadata.GetSilaClientMetadataValue(context.RequestHeaders, _metadataProvider.GetFullyQualifiedMetadataIdentifier("StringMetadata"));
                var stringMetadata = Metadata_StringMetadata.Parser.ParseFrom(value);
                if (stringMetadata?.StringMetadata == null)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.InvalidMetadata, "StringMetadata value could not be parsed (wrong message type)"));
                }

                metadataValue = stringMetadata.StringMetadata.Value;
            }
            catch (Exception e)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.InvalidMetadata, ErrorHandling.HandleException(e)));
            }

            return Task.FromResult(new EchoStringMetadata_Responses { ReceivedStringMetadata = new SiLAFramework.String { Value = metadataValue } });
        }

        public override Task<UnpackMetadata_Responses> UnpackMetadata(UnpackMetadata_Parameters request, ServerCallContext context)
        {
            // get value of StringMetadata entry
            var stringValue = string.Empty;
            var firstIntegerValue = 0;
            var secondIntegerValue = 0;
            try
            {
                // extract metadata object
                byte[] value = SilaClientMetadata.GetSilaClientMetadataValue(context.RequestHeaders, _metadataProvider.GetFullyQualifiedMetadataIdentifier("StringMetadata"));
                var stringMetadata = Metadata_StringMetadata.Parser.ParseFrom(value);
                if (stringMetadata?.StringMetadata == null)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.InvalidMetadata, "StringMetadata value could not be parsed (wrong message type)"));
                }

                value = SilaClientMetadata.GetSilaClientMetadataValue(context.RequestHeaders, _metadataProvider.GetFullyQualifiedMetadataIdentifier("TwoIntegersMetadata"));
                var integerStructureMetadata = Metadata_TwoIntegersMetadata.Parser.ParseFrom(value);
                if (integerStructureMetadata?.TwoIntegersMetadata == null)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.InvalidMetadata, "TwoIntegersMetadata value could not be parsed (wrong message type)"));
                }

                stringValue = stringMetadata.StringMetadata.Value;

                if (integerStructureMetadata.TwoIntegersMetadata.FirstInteger == null)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.InvalidMetadata, "TwoIntegersMetadata.FirstInteger is null"));
                }
                else
                {
                    firstIntegerValue = (int)integerStructureMetadata.TwoIntegersMetadata.FirstInteger.Value;
                }

                if (integerStructureMetadata.TwoIntegersMetadata.SecondInteger == null)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.InvalidMetadata, "TwoIntegersMetadata.SecondInteger is null"));
                }
                else
                {
                    secondIntegerValue = (int)integerStructureMetadata.TwoIntegersMetadata.SecondInteger.Value;
                }
            }
            catch (Exception e)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.InvalidMetadata, ErrorHandling.HandleException(e)));
            }

            return Task.FromResult(new UnpackMetadata_Responses
            {
                ReceivedString = new SiLAFramework.String { Value = stringValue },
                FirstReceivedInteger = new SiLAFramework.Integer { Value = firstIntegerValue },
                SecondReceivedInteger = new SiLAFramework.Integer { Value = secondIntegerValue }
            });
        }

        public override Task<Get_ReceivedStringMetadata_Responses> Get_ReceivedStringMetadata(Get_ReceivedStringMetadata_Parameters request, ServerCallContext context)
        {
            // get value of metadata entry
            var metadataValue = string.Empty;
            try
            {
                // extract metadata object
                byte[] value = SilaClientMetadata.GetSilaClientMetadataValue(context.RequestHeaders, _metadataProvider.GetFullyQualifiedMetadataIdentifier("StringMetadata"));
                var stringMetadata = Metadata_StringMetadata.Parser.ParseFrom(value);
                if (stringMetadata?.StringMetadata == null)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.InvalidMetadata, "StringMetadata value could not be parsed (wrong message type)"));
                }

                metadataValue = stringMetadata.StringMetadata.Value;
            }
            catch (Exception e)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.InvalidMetadata, ErrorHandling.HandleException(e)));
            }

            return Task.FromResult(new Get_ReceivedStringMetadata_Responses { ReceivedStringMetadata = new SiLAFramework.String { Value = metadataValue } });
        }

        public override async Task Subscribe_ReceivedStringMetadataAsCharacters(Subscribe_ReceivedStringMetadataAsCharacters_Parameters request, IServerStreamWriter<Subscribe_ReceivedStringMetadataAsCharacters_Responses> responseStream, ServerCallContext context)
        {
            // get value of metadata entry
            var metadataValue = string.Empty;
            try
            {
                // extract metadata object
                byte[] value = SilaClientMetadata.GetSilaClientMetadataValue(context.RequestHeaders, _metadataProvider.GetFullyQualifiedMetadataIdentifier("StringMetadata"));
                var stringMetadata = Metadata_StringMetadata.Parser.ParseFrom(value);
                if (stringMetadata?.StringMetadata == null)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.InvalidMetadata, "StringMetadata value could not be parsed (wrong message type)"));
                }

                metadataValue = stringMetadata.StringMetadata.Value;
            }
            catch (Exception e)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.InvalidMetadata, ErrorHandling.HandleException(e)));
            }

            var i = 0;
            do
            {
                await responseStream.WriteAsync(new Subscribe_ReceivedStringMetadataAsCharacters_Responses
                {
                    ReceivedStringMetadataAsCharacters = new SiLAFramework.String { Value = metadataValue.Substring(i, 1) }
                });
                await Task.Delay(500);
            } while (++i < metadataValue.Length && !context.CancellationToken.IsCancellationRequested);
        }
    }
}
