﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1;
using SiLA2.Server;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.IntegrationTests.Features.ServiceImplementations
{
    public class UnobservablePropertyTestServiceImpl : UnobservablePropertyTest.UnobservablePropertyTestBase
    {
        private readonly ILogger<UnobservablePropertyTestServiceImpl> _logger;
        private readonly Feature _silaFeature;

        public UnobservablePropertyTestServiceImpl(ISiLA2Server siLA2Server, ILogger<UnobservablePropertyTestServiceImpl> logger)
        {
            _logger = logger;
            _silaFeature = siLA2Server.ReadFeature(Path.Combine("Features", "UnobservablePropertyTest-v1_0.sila.xml"));
        }

        public override Task<Get_AnswerToEverything_Responses> Get_AnswerToEverything(Get_AnswerToEverything_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnswerToEverything_Responses { AnswerToEverything = new SiLAFramework.Integer { Value = 42 } });
        }

        public override Task<Get_SecondsSince1970_Responses> Get_SecondsSince1970(Get_SecondsSince1970_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_SecondsSince1970_Responses
            {
                SecondsSince1970 = new SiLAFramework.Integer { Value = (int)new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds() }
            });
        }
    }
}
