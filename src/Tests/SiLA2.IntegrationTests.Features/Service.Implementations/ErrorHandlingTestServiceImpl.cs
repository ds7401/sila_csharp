﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using SiLA2.Commands;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Test.Errorhandlingtest.V1;
using SiLA2.Server;
using SiLA2.Server.Utils;

namespace SiLA2.IntegrationTests.Features.ServiceImplementations
{
    public class ErrorHandlingTestServiceImpl : ErrorHandlingTest.ErrorHandlingTestBase
    {
        #region Private members

        private readonly Feature _silaFeature;

        private readonly ILogger<BinaryTransferTestServiceImpl> _logger;

        private readonly IObservableCommandManager<RaiseDefinedExecutionErrorObservably_Parameters, RaiseDefinedExecutionErrorObservably_Responses> _definedExecutionErrorCommandManager;
        private readonly IObservableCommandManager<RaiseUndefinedExecutionErrorObservably_Parameters, RaiseUndefinedExecutionErrorObservably_Responses> _undefinedExecutionErrorCommandManager;


        private const string ErrorMessage = "SiLA2_test_error_message";
        private const string DefinedExecutionErrorIdentifier = "TestError";

        #endregion

        #region Constructors and destructors

        public ErrorHandlingTestServiceImpl(
            ISiLA2Server silaServer,
            IObservableCommandManager<RaiseDefinedExecutionErrorObservably_Parameters, RaiseDefinedExecutionErrorObservably_Responses> definedExecutionErrorCommandManager,
            IObservableCommandManager<RaiseUndefinedExecutionErrorObservably_Parameters, RaiseUndefinedExecutionErrorObservably_Responses> undefinedExecutionErrorCommandManager,
            ILogger<BinaryTransferTestServiceImpl> logger)
        {
            _silaFeature = silaServer.ReadFeature(Path.Combine("Features", "ErrorHandlingTest-v1_0.sila.xml"));
            _logger = logger;
            _definedExecutionErrorCommandManager = definedExecutionErrorCommandManager;
            _undefinedExecutionErrorCommandManager = undefinedExecutionErrorCommandManager;
        }

        #endregion

        #region Overrides of ErrorHandlingTestBase

        #region Unobservable Commands

        public override Task<RaiseDefinedExecutionError_Responses> RaiseDefinedExecutionError(RaiseDefinedExecutionError_Parameters request, ServerCallContext context)
        {
            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(_silaFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(DefinedExecutionErrorIdentifier), ErrorMessage));

            return base.RaiseDefinedExecutionError(request, context);
        }

        public override Task<RaiseUndefinedExecutionError_Responses> RaiseUndefinedExecutionError(RaiseUndefinedExecutionError_Parameters request, ServerCallContext context)
        {
            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError(ErrorMessage));

            return base.RaiseUndefinedExecutionError(request, context);
        }

        #endregion

        #region Observable Commands

        public override async Task<CommandConfirmation> RaiseDefinedExecutionErrorObservably(RaiseDefinedExecutionErrorObservably_Parameters request, ServerCallContext context)
        {
            var command = await _definedExecutionErrorCommandManager.AddCommand(request, RaiseDefinedExecutionErrorTask());
            return command.Confirmation;
        }

        public override async Task RaiseDefinedExecutionErrorObservably_Info(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _definedExecutionErrorCommandManager.RegisterForInfo(cmdExecId, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                _logger.LogError($"{e}");
                throw;
            }
        }

        public override Task<RaiseDefinedExecutionErrorObservably_Responses> RaiseDefinedExecutionErrorObservably_Result(CommandExecutionUUID cmdExecId, ServerCallContext context)
        {
            var command = _definedExecutionErrorCommandManager.GetCommand(cmdExecId);
            return Task.FromResult(command.Result);
        }

        private Func<IProgress<ExecutionInfo>, RaiseDefinedExecutionErrorObservably_Parameters, CancellationToken, RaiseDefinedExecutionErrorObservably_Responses> RaiseDefinedExecutionErrorTask()
        {
            return (progress, parameters, cancellationToken) =>
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(_silaFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(DefinedExecutionErrorIdentifier), ErrorMessage));

                return new RaiseDefinedExecutionErrorObservably_Responses();
            };
        }

        public override async Task<CommandConfirmation> RaiseUndefinedExecutionErrorObservably(RaiseUndefinedExecutionErrorObservably_Parameters request, ServerCallContext context)
        {
            var command = await _undefinedExecutionErrorCommandManager.AddCommand(request, RaiseUndefinedExecutionErrorTask());
            return command.Confirmation;
        }

        public override async Task RaiseUndefinedExecutionErrorObservably_Info(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _undefinedExecutionErrorCommandManager.RegisterForInfo(cmdExecId, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                _logger.LogError($"{e}");
                throw;
            }
        }

        public override Task<RaiseUndefinedExecutionErrorObservably_Responses> RaiseUndefinedExecutionErrorObservably_Result(CommandExecutionUUID cmdExecId, ServerCallContext context)
        {
            var command = _undefinedExecutionErrorCommandManager.GetCommand(cmdExecId);
            return Task.FromResult(command.Result);
        }

        private Func<IProgress<ExecutionInfo>, RaiseUndefinedExecutionErrorObservably_Parameters, CancellationToken, RaiseUndefinedExecutionErrorObservably_Responses> RaiseUndefinedExecutionErrorTask()
        {
            return (progress, parameters, cancellationToken) =>
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError(ErrorMessage));

                return new RaiseUndefinedExecutionErrorObservably_Responses();
            };
        }

        #endregion

        #region Properties

        public override Task<Get_RaiseDefinedExecutionErrorOnGet_Responses> Get_RaiseDefinedExecutionErrorOnGet(Get_RaiseDefinedExecutionErrorOnGet_Parameters request, ServerCallContext context)
        {
            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(_silaFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(DefinedExecutionErrorIdentifier), ErrorMessage));

            return base.Get_RaiseDefinedExecutionErrorOnGet(request, context);
        }

        public override Task Subscribe_RaiseDefinedExecutionErrorOnSubscribe(Subscribe_RaiseDefinedExecutionErrorOnSubscribe_Parameters request, IServerStreamWriter<Subscribe_RaiseDefinedExecutionErrorOnSubscribe_Responses> responseStream, ServerCallContext context)
        {
            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(_silaFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(DefinedExecutionErrorIdentifier), ErrorMessage));

            return base.Subscribe_RaiseDefinedExecutionErrorOnSubscribe(request, responseStream, context);
        }

        public override Task<Get_RaiseUndefinedExecutionErrorOnGet_Responses> Get_RaiseUndefinedExecutionErrorOnGet(Get_RaiseUndefinedExecutionErrorOnGet_Parameters request, ServerCallContext context)
        {
            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError(ErrorMessage));

            return base.Get_RaiseUndefinedExecutionErrorOnGet(request, context);
        }

        public override Task Subscribe_RaiseUndefinedExecutionErrorOnSubscribe(Subscribe_RaiseUndefinedExecutionErrorOnSubscribe_Parameters request, IServerStreamWriter<Subscribe_RaiseUndefinedExecutionErrorOnSubscribe_Responses> responseStream, ServerCallContext context)
        {
            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError(ErrorMessage));

            return base.Subscribe_RaiseUndefinedExecutionErrorOnSubscribe(request, responseStream, context);
        }

        public override Task Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent(Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Parameters request, IServerStreamWriter<Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Responses> responseStream, ServerCallContext context)
        {
            // send 1 response
            responseStream.WriteAsync(new Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Responses
            {
                RaiseDefinedExecutionErrorAfterValueWasSent = new Integer { Value = 1 }
            }).Wait();

            // create defined execution error
            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(_silaFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(DefinedExecutionErrorIdentifier), ErrorMessage));

            return base.Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent(request, responseStream, context);
        }

        public override Task Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent(Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Parameters request, IServerStreamWriter<Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Responses> responseStream, ServerCallContext context)
        {
            // send 1 response
            responseStream.WriteAsync(new Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Responses
            {
                RaiseUndefinedExecutionErrorAfterValueWasSent = new Integer { Value = 1 }
            }).Wait();

            // create defined execution error
            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError(ErrorMessage));

            return base.Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent(request, responseStream, context);
        }

        #endregion

        #endregion
    }
}
