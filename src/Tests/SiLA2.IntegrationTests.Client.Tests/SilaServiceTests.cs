﻿using Microsoft.Extensions.Configuration;
using System.Linq;
using SiLA2.Client.Dynamic;
using SiLA2.Server.Utils;
using SiLA2.Utils.Extensions;

namespace SiLA2.IntegrationTests.Client.Tests
{
    [TestFixture]
    public class SilaServiceTests
    {
        const string FEATURE_FILE_NAME = "SiLAService-v1_0.sila.xml";
        private string[] _args = { };
        private IDynamicConfigurator _client;
        private Feature _silaServiceFeature;

        [OneTimeSetUp]
        public void Setup()
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            _client = new DynamicConfigurator(configuration, _args);
            _silaServiceFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("Features", FEATURE_FILE_NAME));
        }

        [TestCase("ServerName", "Test Server")]
        [TestCase("ServerType", "TestServer")]
        [TestCase("ServerDescription", "This is a test server")]
        [TestCase("ServerVersion", "0.1")]
        [TestCase("ServerVendorURL", "https://gitlab.com/SiLA2/sila_interoperability")]
        public async Task Should_Return_Server_Property(string propertyId, string expectedResult)
        {
            // System under Test & Act
            var response = _client.DynamicMessageService.GetUnobservableProperty(propertyId, await _client.GetChannel(), _silaServiceFeature);

            // Assert
            Assert.That(response.GetDynamicMember(propertyId).GetDynamicMember("Value").ToString(), Is.EqualTo(expectedResult));
        }

        [Test]
        public async Task Should_Return_Valid_ServerUUID()
        {
            const string PROPERTY = "ServerUUID";

            // System under Test & Act
            var response = _client.DynamicMessageService.GetUnobservableProperty(PROPERTY, await _client.GetChannel(), _silaServiceFeature);

            // Assert
            var propertyInfo = response.GetType().GetProperty(PROPERTY).GetValue(response, null);
            var propertyValue = propertyInfo.GetType().GetProperty("Value").GetValue(propertyInfo, null);
            if(Guid.TryParse(propertyValue.ToString(), out var uuid))
            {
                Assert.Pass($"Server UUID '{uuid}' is valid.");
            }
            else
            {
                Assert.Fail($"Server  UUID '{propertyValue}' is not valid !");
            }
        }

        [Test]
        public async Task Should_Set_ServerName()
        {
            // Arrange
            const string COMMAND = "SetServerName";
            const string PROPERTY = "ServerName";
            const string EXPECTED_RESULT = "SiLA is Awesome";

            var payloadMap = new Dictionary<string, object> { { PROPERTY, new Sila2.Org.Silastandard.Protobuf.String { Value = EXPECTED_RESULT } } };

            // System under Test & Act
            _client.DynamicMessageService.ExecuteUnobservableCommand(COMMAND, await _client.GetChannel(), _silaServiceFeature, payloadMap);

            // Assert
            var response = _client.DynamicMessageService.GetUnobservableProperty(PROPERTY, await _client.GetChannel(), _silaServiceFeature);
            Assert.That(response.GetDynamicMember(PROPERTY).GetDynamicMember("Value").ToString(), Is.EqualTo(EXPECTED_RESULT));
        }

        [Test]
        public async Task Should_Get_FeatureDefinition()
        {
            // Arrange
            const string COMMAND = "GetFeatureDefinition";
            const string REQUEST_PROPERTY = "FeatureIdentifier";
            const string RESPONSE_PROPERTY = "FeatureDefinition";
            const string REQUEST_PROPERTY_CONTENT = "org.silastandard/core/SiLAService/v1";


            var payloadMap = new Dictionary<string, object> { { REQUEST_PROPERTY, new Sila2.Org.Silastandard.Protobuf.String { Value = REQUEST_PROPERTY_CONTENT } } };

            // System under Test & Act
            var response =  _client.DynamicMessageService.ExecuteUnobservableCommand(COMMAND, await _client.GetChannel(), _silaServiceFeature, payloadMap);

            // Assert
            var silaServiceFeature = FeatureGenerator.ReadFeatureFromXml(response.GetDynamicMember(RESPONSE_PROPERTY).GetDynamicMember("Value").ToString());
            Assert.That(silaServiceFeature.FullyQualifiedIdentifier, Is.EqualTo(REQUEST_PROPERTY_CONTENT));
        }

        [Test]
        public async Task Should_Get_ImplementedFeatures()
        {
            // Arrange
            const string PROPERTY = "ImplementedFeatures";
            const int EXPECTED_COUNT = 13;


            // System under Test & Act
            var response = _client.DynamicMessageService.GetUnobservableProperty(PROPERTY, await _client.GetChannel(), _silaServiceFeature);

            // Assert
            Assert.That(int.Parse(response.GetDynamicMember(PROPERTY).GetDynamicMember("Count").ToString()), Is.EqualTo(EXPECTED_COUNT));
        }
    }
}