﻿using Microsoft.Extensions.Configuration;
using SiLA2.Client.Dynamic;
using SiLA2.Server.Utils;

namespace SiLA2.IntegrationTests.Client.Tests
{
    [TestFixture]
    public class UnobservablePropertyTests
    {
        const string FEATURE_FILE_NAME = "UnobservablePropertyTest-v1_0.sila.xml";
        private string[] _args = { };
        private IDynamicConfigurator _client;
        private Feature _unobservablePropertyFeature;

        [OneTimeSetUp]
        public void Setup()
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            _client = new DynamicConfigurator(configuration, _args);
            _unobservablePropertyFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("Features", FEATURE_FILE_NAME));
        }

        [Test]
        public async Task Should_Return_Unobservable_Property()
        {
            // Arrange
            const string PROPERTY_IDENTIFIER = "AnswerToEverything";
            const long EXPECTED_RESULT = 42;

            // System under Test & Act
            dynamic answerToEverthingResponse = _client.DynamicMessageService.GetUnobservableProperty(PROPERTY_IDENTIFIER, await _client.GetChannel(), _unobservablePropertyFeature);

            // Assert
            Assert.That(answerToEverthingResponse.AnswerToEverything.Value, Is.EqualTo(EXPECTED_RESULT));
        }

        [Test]
        public async Task Should_Return_Seconds_Since_1970_Property()
        {
            // Arrange
            const string PROPERTY_IDENTIFIER = "SecondsSince1970";
            const long PAST_RESULT_20230620 = 1687275752;

            // System under Test & Act
            dynamic answerToEverthingResponse = _client.DynamicMessageService.GetUnobservableProperty(PROPERTY_IDENTIFIER, await _client.GetChannel(), _unobservablePropertyFeature);

            // Assert
            Assert.That(answerToEverthingResponse.SecondsSince1970.Value, Is.GreaterThan(PAST_RESULT_20230620));
        }
    }
}