﻿using Microsoft.Extensions.Configuration;
using SiLA2.Client.Dynamic;
using SiLA2.Server.Utils;

namespace SiLA2.IntegrationTests.Client.Tests
{
    [TestFixture]
    public class ObservablePropertyTests
    {
        const string FEATURE_FILE_NAME = "ObservablePropertyTest-v1_0.sila.xml";
        private string[] _args = { };
        private IDynamicConfigurator _client;
        private Feature _unobservablePropertyFeature;

        [OneTimeSetUp]
        public void Setup()
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            _client = new DynamicConfigurator(configuration, _args);
            _unobservablePropertyFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("Features", FEATURE_FILE_NAME));
        }

        [Test]
        public async Task Should_Return_Observable_Property_With_Fixed_Property()
        {
            // Arrange
            const string PROPERTY_IDENTIFIER = "FixedValue";
            const long EXPECTED_RESULT = 42;

            // System under Test & Act
            var response = _client.DynamicMessageService.SubcribeObservableProperty(PROPERTY_IDENTIFIER, await _client.GetChannel(), _unobservablePropertyFeature);

            var asyncEnumerator = response.GetAsyncEnumerator();

            dynamic responseResultPropertyValue = null;
            while (await asyncEnumerator.MoveNextAsync())
            {
                responseResultPropertyValue = asyncEnumerator.Current;
            }
            // Assert
            Assert.That(responseResultPropertyValue.FixedValue.Value, Is.EqualTo(EXPECTED_RESULT));
        }

        [Test]
        public async Task Should_Return_Observable_Alternating_Property()
        {
            // Arrange
            const string PROPERTY_IDENTIFIER = "Alternating";

            // System under Test & Act
            var response = _client.DynamicMessageService.SubcribeObservableProperty(PROPERTY_IDENTIFIER, await _client.GetChannel(), _unobservablePropertyFeature);

            var asyncEnumerator = response.GetAsyncEnumerator();

            int i = 0;
            dynamic responseResultPropertyValue = null;
            while (await asyncEnumerator.MoveNextAsync())
            {
                responseResultPropertyValue = asyncEnumerator.Current;
                i++;
                if (i > 3)
                    break;
            }
            // Assert
            Assert.That(responseResultPropertyValue.Alternating.Value.GetType(), Is.EqualTo(typeof(bool)));
        }

        [Test]
        public async Task Should_Set_Values_1_2_3_And_Return_Observable_Alternating_Property()
        {
            // Arrange
            const string COMMAND_IDENTIFIER = "SetValue";
            const string PROPERTY_IDENTIFIER = "Editable";

            dynamic responseResultPropertyValue = null;

            for (int j = 1; j < 4; j++)
            {
                var payloadMap = new Dictionary<string, object> { { "Value", new Sila2.Org.Silastandard.Protobuf.Integer { Value = j } } };

                // System under Test & Act
                _ = _client.DynamicMessageService.ExecuteUnobservableCommand(COMMAND_IDENTIFIER, await _client.GetChannel(), _unobservablePropertyFeature, payloadMap);

                var response = _client.DynamicMessageService.SubcribeObservableProperty(PROPERTY_IDENTIFIER, await _client.GetChannel(), _unobservablePropertyFeature);

                var asyncEnumerator = response.GetAsyncEnumerator();

                while (await asyncEnumerator.MoveNextAsync())
                {
                    responseResultPropertyValue = asyncEnumerator.Current;
                    break;
                }

                // Assert
                Assert.That(responseResultPropertyValue.Editable.Value, Is.EqualTo(j));
            }
        }
    }
}