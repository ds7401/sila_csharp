﻿using Microsoft.Extensions.Configuration;
using Sila2.Org.Silastandard;
using SiLA2.Client.Dynamic;
using SiLA2.Server.Utils;

namespace SiLA2.IntegrationTests.Client.Tests
{
    [TestFixture]
    public class ErrorHandlingTests
    {
        const string FEATURE_FILE_NAME = "ErrorHandlingTest-v1_0.sila.xml";
        const string EXPECTED_ERROR_TEXT = "SiLA2_test_error_message";
        private string[] _args = { };
        private IDynamicConfigurator _client;
        private Feature _errorHandlingFeature;

        [OneTimeSetUp]
        public void Setup()
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            _client = new DynamicConfigurator(configuration, _args);
            _errorHandlingFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("Features", FEATURE_FILE_NAME));
        }

        [TestCase("RaiseDefinedExecutionError", "org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError")]
        [TestCase("RaiseUndefinedExecutionError", "undefinedExecutionError")]
        public async Task Should_Throw_ExecutionError_On_Unobservable_Command(string commandIdentifier, string expectedErrorIdentifier)
        {
            // System under Test & Act
            try
            {
                dynamic response = _client.DynamicMessageService.ExecuteUnobservableCommand(commandIdentifier, await _client.GetChannel(), _errorHandlingFeature);
            }
            catch (Exception ex)
            {
                var exception = ErrorHandling.HandleException(ex);
                // Assert
                Assert.That(exception.IndexOf(EXPECTED_ERROR_TEXT) > -1);
                Assert.That(exception.IndexOf(expectedErrorIdentifier) > -1);
                return;
            }
            Assert.Fail();
        }

        [TestCase("RaiseDefinedExecutionErrorObservably", "org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError")]
        [TestCase("RaiseUndefinedExecutionErrorObservably", "undefinedExecutionError")]
        public async Task Should_Throw_ExecutionError_On_Observable_Command(string commandIdentifier, string expectedErrorIdentifier)
        {
            // System under Test & Act
            try
            {
                var response = _client.DynamicMessageService.ExecuteObservableCommand(commandIdentifier, await _client.GetChannel(), _errorHandlingFeature);

                var asyncEnumerator = response.Item2.GetAsyncEnumerator();

                while (await asyncEnumerator.MoveNextAsync())
                {
                    if (asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully
                    || asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError)
                    {
                        break;
                    }
                }

                var result = _client.DynamicMessageService.GetObservableCommandResult(response.Item1.CommandExecutionUUID, commandIdentifier, await _client.GetChannel(), _errorHandlingFeature, response.Item3);
            }
            catch (Exception ex)
            {
                var exception = ErrorHandling.HandleException(ex);
                // Assert
                Assert.That(exception.IndexOf(EXPECTED_ERROR_TEXT) > -1);
                Assert.That(exception.IndexOf(expectedErrorIdentifier) > -1);
                return;
            }
            Assert.Fail();
        }

        [TestCase("RaiseDefinedExecutionErrorOnGet", "org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError")]
        [TestCase("RaiseUndefinedExecutionErrorOnGet", "undefinedExecutionError")]
        public async Task Should_Throw_ExecutionError_On_Unobservable_Property(string propertyId, string errorId)
        {
            // System under Test & Act
            try
            {
                dynamic response = _client.DynamicMessageService.GetUnobservableProperty(propertyId, await _client.GetChannel(), _errorHandlingFeature);
            }
            catch (Exception ex)
            {
                var exception = ErrorHandling.HandleException(ex);
                // Assert
                Assert.That(exception.IndexOf(EXPECTED_ERROR_TEXT) > -1);
                Assert.That(exception.IndexOf(errorId) > -1);
                return;
            }
            Assert.Fail();
        }

        [TestCase("RaiseDefinedExecutionErrorOnSubscribe", "org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError")]
        [TestCase("RaiseUndefinedExecutionErrorOnSubscribe", "undefinedExecutionError")]
        [TestCase("RaiseDefinedExecutionErrorAfterValueWasSent", "org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError")]
        [TestCase("RaiseUndefinedExecutionErrorAfterValueWasSent", "undefinedExecutionError")]
        public async Task Should_Throw_ExecutionError_On_Observable_Property(string propertyId, string errorId)
        {
            // System under Test & Act
            try
            {
                var response = _client.DynamicMessageService.SubcribeObservableProperty(propertyId, await _client.GetChannel(), _errorHandlingFeature);

                var asyncEnumerator = response.GetAsyncEnumerator();

                dynamic responseResultPropertyValue = null;
                while (await asyncEnumerator.MoveNextAsync())
                {
                    responseResultPropertyValue = asyncEnumerator.Current;
                }
            }
            catch (Exception ex)
            {
                var exception = ErrorHandling.HandleException(ex);
                // Assert
                Assert.That(exception.IndexOf(EXPECTED_ERROR_TEXT) > -1);
                Assert.That(exception.IndexOf(errorId) > -1);
                return;
            }
            Assert.Fail();
        }
    }
}