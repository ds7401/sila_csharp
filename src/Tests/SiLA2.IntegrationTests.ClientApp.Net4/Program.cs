﻿using Grpc.Net.Client;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using System;
using System.Net.Http;
using static Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService;

namespace SiLA2.IntegrationTests.ClientApp.Net4
{
    /// <summary>
    /// Windows Server 2022 or Windows 11 or later is required due to their support of HTTP/2.
    /// </summary>
    internal class Program
    {
        static void Main(string[] args)
        {
            var winHttpHandler = new WinHttpHandler();
            winHttpHandler.ServerCertificateValidationCallback = (w,x,y,z) => true;

            var channel = GrpcChannel.ForAddress("https://127.0.0.1:50052", new GrpcChannelOptions
            {
                HttpHandler = winHttpHandler
            });

            try
            {
                Console.WriteLine("Calling .NET Core SiLA2 Server from .NET Framework 4 SiLA2 Client...");
                var silaService = new SiLAServiceClient(channel);
                var response = silaService.Get_ServerName(new Get_ServerName_Parameters());
                Console.WriteLine($"{Environment.NewLine}Response from SiLA2 Server : {response.ServerName}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{Environment.NewLine}Following Exception was thrown{Environment.NewLine}");
                Console.WriteLine($"{ex.Message}");
                throw;
            }
            
            Console.ReadKey();
        }
    }
}
