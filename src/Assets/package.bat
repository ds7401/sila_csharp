@echo off 

set version=8.1.0

cd ../SiLA2.Utils
dotnet restore /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet build /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet pack /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
cd ../SiLA2
dotnet restore /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet build /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet pack /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
cd ../SiLA2.Client
dotnet restore /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet build /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet pack /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
cd ../SiLA2.Client.Dynamic
dotnet restore /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet build /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet pack /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
cd ../SiLA2.Communication
dotnet restore /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet build /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet pack /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
cd ../SiLA2.AnIML
dotnet restore /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet build /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet pack /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
cd ../SiLA2.AspNetCore
dotnet restore /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet build /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet pack /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
cd ../SiLA2.Database.NoSQL
dotnet restore /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet build /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet pack /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
cd ../SiLA2.Database.SQL
dotnet restore /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet build /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet pack /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
cd ../SiLA2.IPC/SiLA2.IPC.NetMQ
dotnet restore /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet build /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet pack /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
cd ../../SiLA2.Server.Frontend/Razor/SiLA2.Frontend.Razor
dotnet restore /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet build /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0
dotnet pack /p:Version=%version% /p:AssemblyVersion=%version%.0 /p:FileVersion=%version%.0