﻿using SiLA2.Server;
using System;
using System.Collections.Generic;
using ConnectionInfo = SiLA2.Network.Discovery.mDNS.ConnectionInfo;

namespace SiLA2.Frontend.Razor.Model
{
    public class ConnectionViewModel : IEquatable<ConnectionViewModel>
    {
        public ConnectionInfo ConnectionInfo { get; }
        public ServerData ServerData { get; }

        public ConnectionViewModel(ConnectionInfo connectionInfo, ServerData serverData)
        {
            ConnectionInfo = connectionInfo;
            ServerData = serverData;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ConnectionViewModel);
        }

        public bool Equals(ConnectionViewModel other)
        {
            return other is not null &&
                   EqualityComparer<ConnectionInfo>.Default.Equals(ConnectionInfo, other.ConnectionInfo);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ConnectionInfo);
        }

        public static bool operator ==(ConnectionViewModel left, ConnectionViewModel right)
        {
            return EqualityComparer<ConnectionViewModel>.Default.Equals(left, right);
        }

        public static bool operator !=(ConnectionViewModel left, ConnectionViewModel right)
        {
            return !(left == right);
        }
    }
}
