﻿using SiLA2.Database.SQL;

namespace SiLA2.Frontend.Razor.Services.UserManagement
{
    public interface IDbUserContext : IDbContext { }
}
