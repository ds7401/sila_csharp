﻿using SiLA2.Frontend.Razor.Model;
using SiLA2.Frontend.Razor.Services;
using System.Collections.Concurrent;

namespace SiLA2.UniversalClient.Net.Services
{
    public class ConnectionViewModelProvider : IConnectionViewModelProvider
    {
        public ConcurrentDictionary<string, ConnectionViewModel> Servers { get; } = new ConcurrentDictionary<string, ConnectionViewModel>();
    }
}
