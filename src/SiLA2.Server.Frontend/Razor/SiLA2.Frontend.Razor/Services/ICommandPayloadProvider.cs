﻿using System;
using System.Collections.Generic;

namespace SiLA2.Frontend.Razor.Services
{
    public interface ICommandPayloadProvider
    {
        IDictionary<string, object> GetPayloadMap(IDictionary<Tuple<string, string, string>, string> parameterMap, FeatureCommand command, Feature feature);
    }
}
