using Microsoft.JSInterop;
using System;
using System.IO;
using System.Threading.Tasks;

namespace SiLA2.Frontend.Razor
{
    // This class provides an example of how JavaScript functionality can be wrapped
    // in a .NET class for easy consumption. The associated JavaScript module is
    // loaded on demand when first needed.
    //
    // This class can be registered as scoped DI service and then injected into Blazor
    // components for use.

    public class FileJsInterop : IAsyncDisposable
    {
        private readonly Lazy<Task<IJSObjectReference>> moduleTask;

        public FileJsInterop(IJSRuntime jsRuntime)
        {
            moduleTask = new(() => jsRuntime.InvokeAsync<IJSObjectReference>(
               "import", "./_content/SiLA2.Frontend.Razor/fileJsInterop.js").AsTask());
        }

        public async ValueTask Download(string fileName, byte[] fileBytes)
        {
            var module = await moduleTask.Value;
            using (var stream = new MemoryStream(fileBytes))
            {
                using var streamRef = new DotNetStreamReference(stream: stream);
                {
                    await module.InvokeVoidAsync("downloadFileFromStream", fileName, streamRef);
                }
            }
        }

        public async ValueTask DisposeAsync()
        {
            if (moduleTask.IsValueCreated)
            {
                var module = await moduleTask.Value;
                await module.DisposeAsync();
            }
        }
    }
}
