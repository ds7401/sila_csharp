﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SiLA2.Database.SQL.Domain
{
    public abstract class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}
