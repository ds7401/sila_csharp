﻿using System.Threading.Tasks;

namespace SiLA2.Client
{
    public interface IBinaryClientService
    {
        Task<string> UploadBinary(byte[] value, int chunkSize, string parameterIdentifier);
        Task<byte[]> DownloadBinary(string binaryTransferUuid);
    }
}