﻿using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;
using System;
using System.Threading.Tasks;

namespace SiLA2.Client
{
    public class BinaryClientService : IBinaryClientService
    {
        private readonly BinaryUpload.BinaryUploadClient _binaryUploadClient;
        private readonly BinaryDownload.BinaryDownloadClient _binaryDownloadClient;
        private readonly ILogger<BinaryClientService> _logger;

        public BinaryClientService(BinaryUpload.BinaryUploadClient binaryUploadClient, BinaryDownload.BinaryDownloadClient binaryDownloadClient, ILogger<BinaryClientService> logger)
        {
            _logger = logger;
            _binaryUploadClient = binaryUploadClient;
            _binaryDownloadClient = binaryDownloadClient;
        }

        public async Task<string> UploadBinary(byte[] value, int chunkSize, string parameterIdentifier)
        {
            try
            {
                var binaryTransferUUID = _binaryUploadClient.CreateBinary(new CreateBinaryRequest
                {
                    BinarySize = (ulong)value.Length,
                    ChunkCount = (uint)(value.Length / chunkSize + (value.Length % chunkSize != 0 ? 1 : 0)),
                    ParameterIdentifier = parameterIdentifier
                }).BinaryTransferUUID;

                using (var call = _binaryUploadClient.UploadChunk())
                {
                    var chunkIndex = 0;
                    var offset = 0;
                    while (offset < value.Length)
                    {
                        var sentChunkSize = Math.Min(chunkSize, value.Length - offset);

                        await call.RequestStream.WriteAsync(new UploadChunkRequest
                        {
                            BinaryTransferUUID = binaryTransferUUID,
                            ChunkIndex = (uint)chunkIndex,
                            Payload = ByteString.CopyFrom(value, offset, sentChunkSize)
                        });

                        await call.ResponseStream.MoveNext();
                        if (call.ResponseStream.Current.BinaryTransferUUID != binaryTransferUUID)
                        {
                            throw new Exception($"Exception while uploading chunk: received binary transfer UUID '{call.ResponseStream.Current.BinaryTransferUUID}' differs from the sent one '{binaryTransferUUID}'");
                        }

                        if (call.ResponseStream.Current.ChunkIndex != chunkIndex)
                        {
                            throw new Exception($"Exception while uploading chunk: received chunk index {call.ResponseStream.Current.ChunkIndex} differs from the sent one ({chunkIndex})");
                        }

                        offset += sentChunkSize;
                        chunkIndex++;
                    }

                    await call.RequestStream.CompleteAsync();

                    _logger.LogInformation($"{value.Length} bytes uploaded with {chunkIndex} chunks");

                    return binaryTransferUUID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ErrorHandling.HandleException(ex));
                throw;
            }
        }

        public Task<byte[]> DownloadBinary(string binaryTransferUuid)
        {
            var binaryInfo = _binaryDownloadClient.GetBinaryInfo(new GetBinaryInfoRequest { BinaryTransferUUID = binaryTransferUuid });
            byte[] buffer = new byte[binaryInfo.BinarySize];
            
            //TODO: Implementation
            using (var call = _binaryDownloadClient.GetChunk())
            {
                //call.RequestStream.WriteAsync(new GetChunkRequest { });

                //call.RequestStream.WriteAsync();
            }
            
            return Task.FromResult(buffer);
        }
    }
}