﻿using Grpc.Net.Client;
using Microsoft.Extensions.DependencyInjection;
using SiLA2.Network.Discovery.mDNS;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using static Sila2.Org.Silastandard.BinaryUpload;

namespace SiLA2.Client
{
    public interface IConfigurator
    {
        IServiceCollection Container { get; }
        IDictionary<Guid, ConnectionInfo> DiscoveredServers { get; }
        IServiceProvider ServiceProvider { get; }
        Task<GrpcChannel> GetChannel(bool acceptAnyServerCertificate = true);
        Task<GrpcChannel> GetChannel(string host, int port, bool acceptAnyServerCertificate = true, X509Certificate2 ca = null);
        Task<IDictionary<Guid, ConnectionInfo>> SearchForServers();
        void UpdateServiceProvider();
    }
}