﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Utils.CmdArgs.Client;
using SiLA2.Utils.Config;
using SiLA2.Utils.Extensions;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace SiLA2.Client
{
    public class Configurator : IConfigurator
    {
        protected ILogger<Configurator> _logger;
        protected IClientConfig _clientConfig;

        public IServiceCollection Container { get; private set; } = new ServiceCollection();

        public IServiceProvider ServiceProvider { get; private set; }

        public IDictionary<Guid, ConnectionInfo> DiscoveredServers { get; private set; } = new Dictionary<Guid, ConnectionInfo>();

        public Configurator(IConfiguration configuration, string[] args)
        {
            SetupContainer(configuration);
            ServiceProvider = Container.BuildServiceProvider();
            args.ParseClientCmdLineArgs<CmdLineClientArgs>(_clientConfig);
            _logger = ServiceProvider.GetService<ILogger<Configurator>>();
        }

        public async Task<IDictionary<Guid, ConnectionInfo>> SearchForServers()
        {
            var servers = await ServiceProvider.GetService<IServiceFinder>().GetConnections(_clientConfig.DiscoveryServiceName, _clientConfig.NetworkInterface, 3000);

            foreach (var server in servers)
            {
                var uuid = Guid.Parse(server.ServerUuid);
                if (!DiscoveredServers.ContainsKey(uuid))
                {
                    DiscoveredServers.Add(uuid, server);
                }
                else
                {
                    DiscoveredServers[uuid] = server;
                }
                _logger.LogInformation($"{server}");
            }

            return DiscoveredServers;
        }

        private void SetupContainer(IConfiguration configuration)
        {
            Container.AddLogging();
            Container.AddSingleton<IServiceFinder, ServiceFinder>();
            Container.AddSingleton<INetworkService, NetworkService>();
            Container.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            _clientConfig = new ClientConfig(configuration);
            Container.AddSingleton(_clientConfig);
        }

        public async Task<GrpcChannel> GetChannel(bool acceptAnyServerCertificate = true)
        {
            return await ServiceProvider.GetService<IGrpcChannelProvider>().GetChannel(_clientConfig.IpOrCdirOrFullyQualifiedHostName, _clientConfig.Port, acceptAnyServerCertificate);
        }

        public async Task<GrpcChannel> GetChannel(string host, int port, bool acceptAnyServerCertificate = true, X509Certificate2 ca = null)
        {
            return await ServiceProvider.GetService<IGrpcChannelProvider>().GetChannel(host, port, acceptAnyServerCertificate, ca);
        }


        public void UpdateServiceProvider()
        {
            ServiceProvider = Container.BuildServiceProvider();
        }
    }
}